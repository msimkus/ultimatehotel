﻿using System.ComponentModel.DataAnnotations;
using Viesbutis.Models.KlientasPosisteme;

namespace Viesbutis.Models
{
    //public class ExternalLoginConfirmationViewModel
    //{
    //    [Required]
    //    [Display(Name = "User name")]
    //    public string UserName { get; set; }
    //}

    public class ChangePasswordViewModel
    {
        [Required(ErrorMessage = "Įveskite senąjį slaptažodį")]
        [DataType(DataType.Password)]
        [Display(Name = "Senasis slaptažodis")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Įveskite naująjį slaptažodį")]
        [StringLength(100, ErrorMessage = "Slaptažodis turi būti mažiausiai {2} simbolių ilgio", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Naujasis slaptažodis")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Pakartokite naująjį slaptažodį")]
        [Compare("NewPassword", ErrorMessage = "Slaptažodžiai nesutampa")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginViewModel
    {
        [Required(ErrorMessage = "Įveskite prisijungimo vardą")]
        [Display(Name = "Prisijungimo vardas")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Įveskite slaptažodį")]
        [DataType(DataType.Password)]
        [Display(Name = "Slaptažodis")]
        public string Password { get; set; }

        //[Display(Name = "Prisiminti slaptažodį?")]
        //public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Šį laukelį būtina užpildyti")]
        [Display(Name = "Vardas")]
        public string vardas { get; set; }

        [Required(ErrorMessage = "Šį laukelį būtina užpildyti")]
        [Display(Name = "Pavardė")]
        public string pavarde { get; set; }

        [Display(Name = "Lytis")]
        public string lytis { get; set; }

        [Required(ErrorMessage = "Pasirinkite miestą")]
        [Display(Name = "Miestas")]
        public int MiestasID { get; set; }

        [Required(ErrorMessage = "Šį laukelį būtina užpildyti")]
        [Display(Name = "Tel. nr.")]
        public string telefono_nr { get; set; }

        [Required(ErrorMessage = "Šį laukelį būtina užpildyti")]
        [Display(Name = "El. paštas")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Netinkamas el. pašto formatas")]
        public string el_pastas { get; set; }

        [Required(ErrorMessage = "Pasirinkite banką")]
        [Display(Name = "Bankas")]
        public int BankasID { get; set; }

        [Required(ErrorMessage = "Šį laukelį būtina užpildyti")]
        [Display(Name = "Kortelės nr.")]
        public string korteles_nr { get; set; }

        [Display(Name = "Teisės")]
        public string teises { get; set; }

        [Display(Name = "Grupė")]
        public int GrupeID { get; set; }

        [Required(ErrorMessage = "Įveskite prisijungimo vardą")]
        [Display(Name = "Prisijungimo vardas")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Įveskite slaptažodį")]
        [StringLength(100, ErrorMessage = "Slaptažodis turi būti mažiausiai {2} simbolių ilgio", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Slaptažodis")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Pakartokite slaptažodį")]
        [Compare("Password", ErrorMessage = "Slaptažodžiai nesutampa")]
        public string ConfirmPassword { get; set; }

        public virtual Bankas Bankas { get; set; }
        public virtual Grupe Grupe { get; set; }
        public virtual Miestas Miestas { get; set; }
    }

    public class EditUserDetailsViewModel
    {
        [Required(ErrorMessage = "Šį laukelį būtina užpildyti")]
        [Display(Name = "Vardas")]
        public string vardas { get; set; }

        [Required(ErrorMessage = "Šį laukelį būtina užpildyti")]
        [Display(Name = "Pavardė")]
        public string pavarde { get; set; }

        [Display(Name = "Lytis")]
        public string lytis { get; set; }

        [Display(Name = "Miestas")]
        public int MiestasID { get; set; }

        [Required(ErrorMessage = "Šį laukelį būtina užpildyti")]
        [Display(Name = "Tel. nr.")]
        public string telefono_nr { get; set; }

        [Required(ErrorMessage = "Šį laukelį būtina užpildyti")]
        [Display(Name = "El. paštas")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Netinkamas el. pašto formatas")]
        public string el_pastas { get; set; }

        [Display(Name = "Bankas")]
        public int BankasID { get; set; }

        [Required(ErrorMessage = "Šį laukelį būtina užpildyti")]
        [Display(Name = "Kortelės nr.")]
        public string korteles_nr { get; set; }

        public virtual Bankas Bankas { get; set; }
        public virtual Miestas Miestas { get; set; }
    }
}
