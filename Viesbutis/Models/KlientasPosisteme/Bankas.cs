﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Viesbutis.Models.KlientasPosisteme
{
    public class Bankas
    {

        public int ID { get; set; }

        public string pavadinimas { get; set; }

        public string kodas { get; set; }

        public string adresas { get; set; }

        public virtual ICollection<Klientas> Klientai { get; set; }
    }
}