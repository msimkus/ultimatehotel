﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Viesbutis.Models.KlientasPosisteme
{
    public class Grupe
    {
        public int ID { get; set; }

        public string pavadinimas { get; set; }

        public double nuolaida { get; set; }

        public string aprasymas { get; set; }

        public virtual ICollection<Klientas> Klientai { get; set; }
    }
}