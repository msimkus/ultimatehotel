﻿using System;
using System.Collections.Generic;
using Viesbutis.Models.RezervacijaPosisteme;

namespace Viesbutis.Models.KlientasPosisteme
{
    public class Klientas
    {
        public int ID { get; set; }

        public int miestasID { get; set; }

        public int grupeID { get; set; }

        public int bankasID { get; set; }

        public string vardas { get; set; }

        public string pavarde { get; set; }

        public string lytis { get; set; }

        public string prisijungimo_vardas { get; set; }

        public string slaptazodis { get; set; }

        public string teises { get; set; }

        public string el_pastas { get; set; }

        public string korteles_nr { get; set; }

        public string telefono_nr { get; set; }

        public virtual Miestas Miestas { get; set; }

        public virtual Grupe Grupe { get; set; }

        public virtual Bankas Bankas { get; set; }

        public virtual ICollection<Rezervacija> Rezervacijos { get; set; }

    }
}
