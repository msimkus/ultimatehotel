﻿using System;
using System.Collections.Generic;

namespace Viesbutis.Models.KlientasPosisteme
{
    public class Miestas
    {

        public int ID { get; set; }
        public string pavadinimas { get; set; }
        public int kodas { get; set; }
        public virtual ICollection<Klientas> Klientai { get; set; }
    }
}
