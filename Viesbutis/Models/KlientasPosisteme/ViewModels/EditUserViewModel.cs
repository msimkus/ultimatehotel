﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Viesbutis.Models.KlientasPosisteme.ViewModels
{
    public class EditUserViewModel
    {
        public int ID { get; set; }

        [Display(Name = "Grupė")]
        public int GrupeID { get; set; }

        [Display(Name = "Teisės")]
        public string teises { get; set; }

        public virtual Grupe Grupe { get; set; }
    }
}