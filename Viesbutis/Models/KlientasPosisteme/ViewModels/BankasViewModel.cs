﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Viesbutis.Models.KlientasPosisteme.ViewModels
{
    public class BankasViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Pavadinimas")]
        public string pavadinimas { get; set; }

        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Kodas")]
        public string kodas { get; set; }

        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Adresas")]
        public string adresas { get; set; }

        public virtual ICollection<Klientas> Klientai { get; set; }
    }
}