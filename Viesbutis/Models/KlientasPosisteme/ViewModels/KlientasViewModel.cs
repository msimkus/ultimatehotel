﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Viesbutis.Models.RezervacijaPosisteme;

namespace Viesbutis.Models.KlientasPosisteme.ViewModels
{
    public class KlientasViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Šį laukelį būtina užpildyti")]
        [Display(Name = "Vardas")]
        public string vardas { get; set; }

        [Required(ErrorMessage = "Šį laukelį būtina užpildyti")]
        [Display(Name = "Pavardė")]
        public string pavarde { get; set; }

        [Display(Name = "Lytis")]
        public string lytis { get; set; }

        [Display(Name = "Miestas")]
        public int MiestasID { get; set; }

        [Required(ErrorMessage = "Šį laukelį būtina užpildyti")]
        [Display(Name = "Tel. nr.")]
        public string telefono_nr { get; set; }

        [Required(ErrorMessage = "Šį laukelį būtina užpildyti")]
        [Display(Name = "El. paštas")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Netinkamas el. pašto formatas")]
        public string el_pastas { get; set; }

        [Display(Name = "Bankas")]
        public int BankasID { get; set; }

        [Required(ErrorMessage = "Šį laukelį būtina užpildyti")]
        [Display(Name = "Kortelės nr.")]
        public string korteles_nr { get; set; }

        [Display(Name = "Teisės")]
        public string teises { get; set; }

        [Display(Name = "Grupė")]
        public int GrupeID { get; set; }

        public virtual Bankas Bankas { get; set; }
        public virtual Grupe Grupe { get; set; }
        public virtual Miestas Miestas { get; set; }
    }
}