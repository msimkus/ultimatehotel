﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Viesbutis.Models.KlientasPosisteme.ViewModels
{
    public class GrupeViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Pavadinimas")]
        public string pavadinimas { get; set; }

        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Nuolaida")]
        public double nuolaida { get; set; }

        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Aprašymas")]
        public string aprasymas { get; set; }

        public virtual ICollection<Klientas> Klientai { get; set; }
    }
}