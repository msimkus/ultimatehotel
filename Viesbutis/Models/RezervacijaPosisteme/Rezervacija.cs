﻿using System;
using System.Collections.Generic;
using Viesbutis.Models.KambarysPosisteme;
using Viesbutis.Models.KlientasPosisteme;
using Viesbutis.Models.MokejimasPosisteme;

namespace Viesbutis.Models.RezervacijaPosisteme
{
    public class Rezervacija
    {
        public int ID { get; set; }
        public int klientasID { get; set; }
        public int mokejimasID { get; set; }
        public int kambarysID { get; set; }
        public int rezervacijoslaikasID { get; set; }
        public Boolean patvirtinta { get; set; }

        public virtual Klientas Klientas { get; set; }
        public virtual Mokejimas Mokejimas { get; set; }
        public virtual Kambarys Kambarys { get; set; }
        public virtual RezervacijosLaikas RezervacijosLaikas { get; set; }
        public virtual ICollection<PaslaugosTarpine> PaslaugosTarpines { get; set; }

    }
}