﻿using System;
using System.Collections.Generic;

namespace Viesbutis.Models.RezervacijaPosisteme
{
    public class RezervacijosLaikas
    {

        public int ID { get; set; }
        public int kambarysID { get; set; }
        public DateTime rezervuota_nuo { get; set; }
        public DateTime rezervuota_iki { get; set; }
        public Boolean uzimta { get; set; }
        public string rezervuota_nuo_iki
        {
            get { return string.Format("{0} --- {1}", rezervuota_nuo, rezervuota_iki); }
            set { }
        }
        // public virtual Kambarys Kambarys { get; set; }
        public virtual ICollection<Rezervacija> Rezervacijos { get; set; }
    }
}