﻿using System;
using System.Collections.Generic;

namespace Viesbutis.Models.RezervacijaPosisteme
{
    public class Paslauga
    {

        public int ID { get; set; }
        public string pavadinimas { get; set; }
        public string aprasymas { get; set; }
        public double kaina { get; set; }
        public virtual ICollection<PaslaugosTarpine> PaslaugosTarpines { get; set; }

    }
}