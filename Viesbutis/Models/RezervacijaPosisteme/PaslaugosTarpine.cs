﻿using System;
using System.Collections.Generic;

namespace Viesbutis.Models.RezervacijaPosisteme
{
    public class PaslaugosTarpine
    {
        public int ID { get; set; }
        public int rezervacijaID { get; set; }
        public int paslaugaID { get; set; }
        public virtual Rezervacija Rezervacija { get; set; }
        public virtual Paslauga Paslauga { get; set; }

    }
}