﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Viesbutis.Models.RezervacijaPosisteme;

namespace Viesbutis.Models.MokejimasPosisteme
{
    public class Ataskaita
    {
        [Display(Name = "Klientas")]
        public string klientas { get; set; }

        [Display(Name = "Data")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime mokejimo_data { get; set; }

        [Display(Name = "Mokėjimo suma")]
        public double mokejimo_suma { get; set; }

        [Display(Name = "Mokėjimo būsena")]
        public Boolean mokejimo_busena { get; set; }

        [Display(Name = "Aptarnavo")]
        public string kas_aptarnavo { get; set; }

        [Display(Name = "Mokėjimo valiuta")]
        public string mokejimo_valiuta { get; set; }

        [Display(Name = "Mokėjimo tipas")]
        public string mokejimo_tipas { get; set; }
    }
}
