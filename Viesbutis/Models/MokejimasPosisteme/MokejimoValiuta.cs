﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Viesbutis.Models.MokejimasPosisteme
{
    public class MokejimoValiuta
    {

        public int ID { get; set; }

        [Display(Name = "Pavadinimas")]
        public string pavadinimas { get; set; }

        [Display(Name = "Kursas")]
        public double kursas { get; set; }
        public virtual ICollection<Mokejimas> Mokejimai { get; set; }
    }
}