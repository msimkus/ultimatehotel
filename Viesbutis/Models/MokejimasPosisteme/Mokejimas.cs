﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Viesbutis.Models.RezervacijaPosisteme;

namespace Viesbutis.Models.MokejimasPosisteme
{
    public class Mokejimas
    {
        public int ID { get; set; }
        public int mokejimotipasID { get; set; }
        public int mokejimovaliutaID { get; set; }

        [Display(Name = "Data")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime mokejimo_data { get; set; }

        [Display(Name = "Mokėjimo suma")]
        public double mokejimo_suma { get; set; }

        [Display(Name = "Mokėjimo būsena")]
        public Boolean mokejimo_busena { get; set; }

        [Display(Name="Klientas")]
        public string klientas { get; set; }

        [Display(Name = "Aptarnavo")]
        public string kas_aptarnavo { get; set; }    
        public virtual MokejimoTipas MokejimoTipas { get; set; }
        public virtual MokejimoValiuta MokejimoValiuta { get; set; }
        public virtual ICollection<Rezervacija> Rezervacijos { get; set; }


    }
}
