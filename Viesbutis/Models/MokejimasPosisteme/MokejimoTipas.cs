﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Viesbutis.Models.MokejimasPosisteme
{
    public class MokejimoTipas
    {

        public int ID { get; set; }

        //[Required(ErrorMessage = "Užpildykite laukelį"),
        [Display(Name = "Pavadinimas")]
        public string pavadinimas { get; set; }
        public virtual ICollection<Mokejimas> Mokejimai { get; set; }
    }
}