﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Viesbutis.Models.KambarysPosisteme
{
    public class PatogumaiTarpine
    {
        public int ID { get; set; }
        
        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Kambarys")]
        public int kambarysID { get; set; }

        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Patogumas")]
        public int patogumasID { get; set; }
      
       public virtual Kambarys Kambarys { get; set; }
        public virtual Patogumas Patogumas { get; set; }

    }
}
