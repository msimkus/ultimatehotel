﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Viesbutis.Models.KambarysPosisteme
{
    public class Ivertinimas
    {

        public int ID { get; set; }

        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Kambarys")]
        public int kambarysID { get; set; }

        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Įvertinimas")]
        [Range(1, 5)]
        public int vertinimas { get; set; }

        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Komentaras")]
        [StringLength(500, MinimumLength = 1)]
        public string komentaras { get; set; }

        public virtual Kambarys Kambarys { get; set; }
    }
}