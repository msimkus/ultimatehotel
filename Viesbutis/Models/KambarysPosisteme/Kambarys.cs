﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Viesbutis.Models.RezervacijaPosisteme;



namespace Viesbutis.Models.KambarysPosisteme
{
    
    public class Kambarys
    {
        
        [Display(Name = "Kambarys")]
        public int ID { get; set; }
        
        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Apgyvendinimo tipas")]
        public int apgyvendinimoTipasID { get; set; }
        
        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Kambario tipas")]
        public int kambarioTipasID { get; set; }
        
        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Kambario numeris")]
        public int numeris { get; set; }
        
        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Aukštas")]
        [Range(1, 10)]
        public int aukstas { get; set; }

        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Kambario plotas")]
        [Range(4, 100)]
        public double plotas { get; set; }

        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Kaina")]
        [Range(10, 10000)]
        public double kaina { get; set; }  
        public virtual KambarioTipas KambarioTipas { get; set; }
        public virtual ApgyvendinimoTipas ApgyvendinimoTipas { get; set; }
        public virtual ICollection<Ivertinimas> Ivertinimai { get; set; }
        public virtual ICollection<Rezervacija> Rezervacijos { get; set; }
        public virtual ICollection<PatogumaiTarpine> Patogumai { get; set; }


    }
}
