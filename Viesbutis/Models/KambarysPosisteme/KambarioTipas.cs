﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Viesbutis.Models.KambarysPosisteme
{
    public class KambarioTipas
    {

        public int ID { get; set; }

       [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Kambario tipas (trumpinys)")]
       [StringLength(50, MinimumLength = 3)]
        public string trumpinys { get; set; }

        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Kambario tipas")]
        [StringLength(50, MinimumLength = 3)]
        public string pavadinimas { get; set; }

       [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Parašymas")]
       [StringLength(500, MinimumLength = 3)]
        public string aprasymas { get; set; }
        public virtual ICollection<Kambarys> Kambariai { get; set; }
    }
}