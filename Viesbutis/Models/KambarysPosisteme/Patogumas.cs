﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Viesbutis.Models.KambarysPosisteme
{
    public class Patogumas
    {

        public int ID { get; set; }
       
        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Patogumas")]
        [StringLength(50, MinimumLength = 3)]
        public string pavadinimas { get; set; }
        public virtual ICollection<PatogumaiTarpine> PatogumaiTarpines { get; set; }
    }
}