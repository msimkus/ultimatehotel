﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Viesbutis.Models.RezervacijaPosisteme;
using System.Linq;
using System.Web;



namespace Viesbutis.Models.KambarysPosisteme.ViewModels
{

    public class KambarysViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Apgyvendinimo tipas")]
        public int apgyvendinimoTipasID { get; set; }
        
        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Kambario tipas")]
        public int kambarioTipasID { get; set; }

        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Kambario numeris")]
        public int numeris { get; set; }

        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Aukštas")]
        public int aukstas { get; set; }

        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Kambario plotas")]
        public double plotas { get; set; }

        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Kaina už vieną parą")]
        public double kaina { get; set; }
        public virtual KambarioTipas KambarioTipas { get; set; }
        public virtual ApgyvendinimoTipas ApgyvendinimoTipas { get; set; }
        public virtual ICollection<Ivertinimas> Ivertinimai { get; set; }
        public virtual ICollection<Rezervacija> Rezervacijos { get; set; }


    }
}