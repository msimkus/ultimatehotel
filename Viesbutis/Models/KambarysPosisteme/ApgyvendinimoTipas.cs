﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Viesbutis.Models.KambarysPosisteme
{
    public class ApgyvendinimoTipas
    {

        public int ID { get; set; }

        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Apgyvendinimo tipas")]
        [StringLength(50, MinimumLength = 3)]
        public string pavadinimas { get; set; }

        [Required(ErrorMessage = "Užpildykite laukelį"),
        Display(Name = "Aprašymas")]
        [StringLength(500, MinimumLength = 3)]
        public string aprasymas { get; set; }
        public virtual ICollection<Kambarys> Kambariai { get; set; }
    }
}