namespace Viesbutis.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedatabase : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ApgyvendinimoTipas", "pavadinimas", c => c.String(nullable: false));
            AlterColumn("dbo.ApgyvendinimoTipas", "aprasymas", c => c.String(nullable: false));
            AlterColumn("dbo.KambarioTipas", "trumpinys", c => c.String(nullable: false));
            AlterColumn("dbo.KambarioTipas", "pavadinimas", c => c.String(nullable: false));
            AlterColumn("dbo.KambarioTipas", "aprasymas", c => c.String(nullable: false));
            AlterColumn("dbo.Patogumas", "pavadinimas", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Patogumas", "pavadinimas", c => c.String());
            AlterColumn("dbo.KambarioTipas", "aprasymas", c => c.String());
            AlterColumn("dbo.KambarioTipas", "pavadinimas", c => c.String());
            AlterColumn("dbo.KambarioTipas", "trumpinys", c => c.String());
            AlterColumn("dbo.ApgyvendinimoTipas", "aprasymas", c => c.String());
            AlterColumn("dbo.ApgyvendinimoTipas", "pavadinimas", c => c.String());
        }
    }
}
