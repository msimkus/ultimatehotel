namespace Viesbutis.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migration2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ivertinimas", "vertinimas", c => c.Int(nullable: false));
            DropColumn("dbo.Ivertinimas", "ivertinimas");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Ivertinimas", "ivertinimas", c => c.Double(nullable: false));
            DropColumn("dbo.Ivertinimas", "vertinimas");
        }
    }
}
