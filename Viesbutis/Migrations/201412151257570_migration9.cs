namespace Viesbutis.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migration9 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RezervacijosLaikas", "rezervuota_nuo_iki", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.RezervacijosLaikas", "rezervuota_nuo_iki");
        }
    }
}
