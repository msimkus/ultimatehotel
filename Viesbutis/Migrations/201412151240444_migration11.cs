namespace Viesbutis.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migration11 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ApgyvendinimoTipas", "pavadinimas", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.ApgyvendinimoTipas", "aprasymas", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Ivertinimas", "komentaras", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.KambarioTipas", "trumpinys", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.KambarioTipas", "pavadinimas", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.KambarioTipas", "aprasymas", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Patogumas", "pavadinimas", c => c.String(nullable: false, maxLength: 50));
           // DropColumn("dbo.RezervacijosLaikas", "rezervuota_nuo_iki");
        }
        
        public override void Down()
        {
            //AddColumn("dbo.RezervacijosLaikas", "rezervuota_nuo_iki", c => c.String());
            AlterColumn("dbo.Patogumas", "pavadinimas", c => c.String(nullable: false));
            AlterColumn("dbo.KambarioTipas", "aprasymas", c => c.String(nullable: false));
            AlterColumn("dbo.KambarioTipas", "pavadinimas", c => c.String(nullable: false));
            AlterColumn("dbo.KambarioTipas", "trumpinys", c => c.String(nullable: false));
            AlterColumn("dbo.Ivertinimas", "komentaras", c => c.String(nullable: false));
            AlterColumn("dbo.ApgyvendinimoTipas", "aprasymas", c => c.String(nullable: false));
            AlterColumn("dbo.ApgyvendinimoTipas", "pavadinimas", c => c.String(nullable: false));
        }
    }
}
