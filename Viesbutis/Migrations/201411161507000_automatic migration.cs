namespace Viesbutis.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class automaticmigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Kambarys", "ivertinimasID", "dbo.Ivertinimas");
            DropIndex("dbo.Kambarys", new[] { "ivertinimasID" });
            AddColumn("dbo.Ivertinimas", "kambarysID", c => c.Int(nullable: false));
            CreateIndex("dbo.Ivertinimas", "kambarysID");
            AddForeignKey("dbo.Ivertinimas", "kambarysID", "dbo.Kambarys", "ID", cascadeDelete: true);
            DropColumn("dbo.Kambarys", "ivertinimasID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Kambarys", "ivertinimasID", c => c.Int(nullable: false));
            DropForeignKey("dbo.Ivertinimas", "kambarysID", "dbo.Kambarys");
            DropIndex("dbo.Ivertinimas", new[] { "kambarysID" });
            DropColumn("dbo.Ivertinimas", "kambarysID");
            CreateIndex("dbo.Kambarys", "ivertinimasID");
            AddForeignKey("dbo.Kambarys", "ivertinimasID", "dbo.Ivertinimas", "ID", cascadeDelete: true);
        }
    }
}
