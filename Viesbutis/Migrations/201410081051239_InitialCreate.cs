namespace Viesbutis.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApgyvendinimoTipas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        pavadinimas = c.String(),
                        aprasymas = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Kambarys",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        apgyvendinimoTipasID = c.Int(nullable: false),
                        ivertinimasID = c.Int(nullable: false),
                        kambarioTipasID = c.Int(nullable: false),
                        numeris = c.Int(nullable: false),
                        aukstas = c.Int(nullable: false),
                        plotas = c.Double(nullable: false),
                        kaina = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ApgyvendinimoTipas", t => t.apgyvendinimoTipasID, cascadeDelete: true)
                .ForeignKey("dbo.Ivertinimas", t => t.ivertinimasID, cascadeDelete: true)
                .ForeignKey("dbo.KambarioTipas", t => t.kambarioTipasID, cascadeDelete: true)
                .Index(t => t.apgyvendinimoTipasID)
                .Index(t => t.ivertinimasID)
                .Index(t => t.kambarioTipasID);
            
            CreateTable(
                "dbo.Ivertinimas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ivertinimas = c.Double(nullable: false),
                        komentaras = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.KambarioTipas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        trumpinys = c.String(),
                        pavadinimas = c.String(),
                        aprasymas = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Rezervacija",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        klientasID = c.Int(nullable: false),
                        mokejimasID = c.Int(nullable: false),
                        kambarysID = c.Int(nullable: false),
                        rezervacijoslaikasID = c.Int(nullable: false),
                        patvirtinta = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Kambarys", t => t.kambarysID, cascadeDelete: true)
                .ForeignKey("dbo.Klientas", t => t.klientasID, cascadeDelete: true)
                .ForeignKey("dbo.Mokejimas", t => t.mokejimasID, cascadeDelete: true)
                .ForeignKey("dbo.RezervacijosLaikas", t => t.rezervacijoslaikasID, cascadeDelete: true)
                .Index(t => t.klientasID)
                .Index(t => t.mokejimasID)
                .Index(t => t.kambarysID)
                .Index(t => t.rezervacijoslaikasID);
            
            CreateTable(
                "dbo.Klientas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        miestasID = c.Int(nullable: false),
                        grupeID = c.Int(nullable: false),
                        bankasID = c.Int(nullable: false),
                        vardas = c.String(),
                        pavarde = c.String(),
                        lytis = c.String(),
                        el_pastas = c.String(),
                        korteles_nr = c.String(),
                        telefono_nr = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Bankas", t => t.bankasID, cascadeDelete: true)
                .ForeignKey("dbo.Grupe", t => t.grupeID, cascadeDelete: true)
                .ForeignKey("dbo.Miestas", t => t.miestasID, cascadeDelete: true)
                .Index(t => t.miestasID)
                .Index(t => t.grupeID)
                .Index(t => t.bankasID);
            
            CreateTable(
                "dbo.Bankas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        pavadinimas = c.String(),
                        adresas = c.String(),
                        kodas = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Grupe",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        pavadinimas = c.String(),
                        nuolaida = c.Double(nullable: false),
                        aprasymas = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Miestas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        pavadinimas = c.String(),
                        kodas = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Mokejimas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        mokejimotipasID = c.Int(nullable: false),
                        mokejimovaliutaID = c.Int(nullable: false),
                        mokejimo_data = c.DateTime(nullable: false),
                        mokejimo_suma = c.Double(nullable: false),
                        mokejimo_busena = c.Boolean(nullable: false),
                        kas_aptarnavo = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.MokejimoTipas", t => t.mokejimotipasID, cascadeDelete: true)
                .ForeignKey("dbo.MokejimoValiuta", t => t.mokejimovaliutaID, cascadeDelete: true)
                .Index(t => t.mokejimotipasID)
                .Index(t => t.mokejimovaliutaID);
            
            CreateTable(
                "dbo.MokejimoTipas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        pavadinimas = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MokejimoValiuta",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        pavadinimas = c.String(),
                        kursas = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PaslaugosTarpine",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        rezervacijaID = c.Int(nullable: false),
                        paslaugaID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Paslauga", t => t.paslaugaID, cascadeDelete: true)
                .ForeignKey("dbo.Rezervacija", t => t.rezervacijaID, cascadeDelete: true)
                .Index(t => t.rezervacijaID)
                .Index(t => t.paslaugaID);
            
            CreateTable(
                "dbo.Paslauga",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        pavadinimas = c.String(),
                        aprasymas = c.String(),
                        kaina = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.RezervacijosLaikas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        kambarysID = c.Int(nullable: false),
                        rezervuota_nuo = c.DateTime(nullable: false),
                        rezervuota_iki = c.DateTime(nullable: false),
                        uzimta = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Patogumas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        pavadinimas = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PatogumaiTarpine",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        kambarysID = c.Int(nullable: false),
                        patogumasID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Kambarys", t => t.kambarysID, cascadeDelete: true)
                .ForeignKey("dbo.Patogumas", t => t.patogumasID, cascadeDelete: true)
                .Index(t => t.kambarysID)
                .Index(t => t.patogumasID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PatogumaiTarpine", "patogumasID", "dbo.Patogumas");
            DropForeignKey("dbo.PatogumaiTarpine", "kambarysID", "dbo.Kambarys");
            DropForeignKey("dbo.Rezervacija", "rezervacijoslaikasID", "dbo.RezervacijosLaikas");
            DropForeignKey("dbo.PaslaugosTarpine", "rezervacijaID", "dbo.Rezervacija");
            DropForeignKey("dbo.PaslaugosTarpine", "paslaugaID", "dbo.Paslauga");
            DropForeignKey("dbo.Rezervacija", "mokejimasID", "dbo.Mokejimas");
            DropForeignKey("dbo.Mokejimas", "mokejimovaliutaID", "dbo.MokejimoValiuta");
            DropForeignKey("dbo.Mokejimas", "mokejimotipasID", "dbo.MokejimoTipas");
            DropForeignKey("dbo.Rezervacija", "klientasID", "dbo.Klientas");
            DropForeignKey("dbo.Klientas", "miestasID", "dbo.Miestas");
            DropForeignKey("dbo.Klientas", "grupeID", "dbo.Grupe");
            DropForeignKey("dbo.Klientas", "bankasID", "dbo.Bankas");
            DropForeignKey("dbo.Rezervacija", "kambarysID", "dbo.Kambarys");
            DropForeignKey("dbo.Kambarys", "kambarioTipasID", "dbo.KambarioTipas");
            DropForeignKey("dbo.Kambarys", "ivertinimasID", "dbo.Ivertinimas");
            DropForeignKey("dbo.Kambarys", "apgyvendinimoTipasID", "dbo.ApgyvendinimoTipas");
            DropIndex("dbo.PatogumaiTarpine", new[] { "patogumasID" });
            DropIndex("dbo.PatogumaiTarpine", new[] { "kambarysID" });
            DropIndex("dbo.PaslaugosTarpine", new[] { "paslaugaID" });
            DropIndex("dbo.PaslaugosTarpine", new[] { "rezervacijaID" });
            DropIndex("dbo.Mokejimas", new[] { "mokejimovaliutaID" });
            DropIndex("dbo.Mokejimas", new[] { "mokejimotipasID" });
            DropIndex("dbo.Klientas", new[] { "bankasID" });
            DropIndex("dbo.Klientas", new[] { "grupeID" });
            DropIndex("dbo.Klientas", new[] { "miestasID" });
            DropIndex("dbo.Rezervacija", new[] { "rezervacijoslaikasID" });
            DropIndex("dbo.Rezervacija", new[] { "kambarysID" });
            DropIndex("dbo.Rezervacija", new[] { "mokejimasID" });
            DropIndex("dbo.Rezervacija", new[] { "klientasID" });
            DropIndex("dbo.Kambarys", new[] { "kambarioTipasID" });
            DropIndex("dbo.Kambarys", new[] { "ivertinimasID" });
            DropIndex("dbo.Kambarys", new[] { "apgyvendinimoTipasID" });
            DropTable("dbo.PatogumaiTarpine");
            DropTable("dbo.Patogumas");
            DropTable("dbo.RezervacijosLaikas");
            DropTable("dbo.Paslauga");
            DropTable("dbo.PaslaugosTarpine");
            DropTable("dbo.MokejimoValiuta");
            DropTable("dbo.MokejimoTipas");
            DropTable("dbo.Mokejimas");
            DropTable("dbo.Miestas");
            DropTable("dbo.Grupe");
            DropTable("dbo.Bankas");
            DropTable("dbo.Klientas");
            DropTable("dbo.Rezervacija");
            DropTable("dbo.KambarioTipas");
            DropTable("dbo.Ivertinimas");
            DropTable("dbo.Kambarys");
            DropTable("dbo.ApgyvendinimoTipas");
        }
    }
}
