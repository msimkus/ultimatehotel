namespace Viesbutis.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class new0 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Ivertinimas", "komentaras", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Ivertinimas", "komentaras", c => c.String());
        }
    }
}
