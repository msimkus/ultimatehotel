namespace Viesbutis.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migration29 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Klientas", "prisijungimo_vardas", c => c.String());
            AddColumn("dbo.Klientas", "slaptazodis", c => c.String());
            AddColumn("dbo.Klientas", "teises", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Klientas", "teises");
            DropColumn("dbo.Klientas", "slaptazodis");
            DropColumn("dbo.Klientas", "prisijungimo_vardas");
        }
    }
}
