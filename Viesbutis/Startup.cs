﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Viesbutis.Startup))]
namespace Viesbutis
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
