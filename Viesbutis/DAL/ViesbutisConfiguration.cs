﻿using System.Data.Entity;
using System.Data.Entity.SqlServer;

namespace Viesbutis.DAL
{
    public class ViesbutisConfiguration : DbConfiguration
    {
        public ViesbutisConfiguration()
        {
            SetExecutionStrategy("System.Data.SqlClient", () => new SqlAzureExecutionStrategy());
        }
    }
}