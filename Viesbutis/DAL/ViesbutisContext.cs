﻿using Viesbutis.Models.KambarysPosisteme;
using Viesbutis.Models.KlientasPosisteme;
using Viesbutis.Models.MokejimasPosisteme;
using Viesbutis.Models.RezervacijaPosisteme;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Viesbutis.DAL
{
    public class ViesbutisContext : DbContext
    {

        public ViesbutisContext()
            : base("ViesbutisContext")
        {
        }

        public DbSet<ApgyvendinimoTipas> ApgyvendinimoTipai { get; set; }
        public DbSet<Ivertinimas> Ivertinimai { get; set; }
        public DbSet<KambarioTipas> KambarioTipai { get; set; }
        public DbSet<Kambarys> Kambariai { get; set; }
        public DbSet<PatogumaiTarpine> PatogumaiTarpines { get; set; }
        public DbSet<Patogumas> Patogumai { get; set; }

        public DbSet<Klientas> Klientai { get; set; }
        public DbSet<Miestas> Miestai { get; set; }
        public DbSet<Grupe> Grupes { get; set; }
        public DbSet<Bankas> Bankai { get; set; }

        public DbSet<Mokejimas> Mokejimai { get; set; }
        public DbSet<MokejimoTipas> MokejimoTipai { get; set; }
        public DbSet<MokejimoValiuta> MokejimoValiutos { get; set; }

        public DbSet<Paslauga> Paslaugos { get; set; }
        public DbSet<PaslaugosTarpine> PaslaugosTarpines { get; set; }
        public DbSet<Rezervacija> Rezervacijos { get; set; }
        public DbSet<RezervacijosLaikas> RezervacijosLaikai { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}