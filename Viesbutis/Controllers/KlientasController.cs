﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Viesbutis.Models.KlientasPosisteme;
using Viesbutis.DAL;
using System.Data.Entity.Infrastructure;
using Viesbutis.Models.KlientasPosisteme.ViewModels;

namespace Viesbutis.Controllers
{
    public class KlientasController : Controller
    {
        private ViesbutisContext _db = new ViesbutisContext();

        // GET: /Klientas/
        [Authorize(Roles = "Administratorius")]
        public ActionResult Index()
        {
            var klientai = _db.Klientai.OrderBy(x => x.vardas).ThenBy(x => x.pavarde);

            return View(klientai);
        }

        // GET: /Klientas/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Klientas klientas = _db.Klientai.Find(id);
            if (klientas == null)
            {
                return HttpNotFound();
            }

            KlientasViewModel model = new KlientasViewModel 
            {
                ID = klientas.ID,
                vardas = klientas.vardas,
                pavarde = klientas.pavarde,
                lytis = klientas.lytis,
                Miestas = klientas.Miestas,
                telefono_nr = klientas.telefono_nr,
                el_pastas = klientas.el_pastas,
                Bankas = klientas.Bankas,
                korteles_nr = klientas.korteles_nr,
                Grupe = klientas.Grupe,
                teises = klientas.teises
            };

            return View(model);
        }

        // GET: /Klientas/EditUserGroup/5
        [Authorize(Roles = "Administratorius")]
        public ActionResult EditUserGroup(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Klientas klientas = _db.Klientai.Find(id);
            if (klientas == null)
            {
                return HttpNotFound();
            }

            EditUserViewModel model = new EditUserViewModel 
            {
                ID = klientas.ID,
                GrupeID = klientas.grupeID
            };

            ViewBag.GrupesID = new SelectList(_db.Grupes.OrderBy(m => m.pavadinimas), "ID", "pavadinimas", model.GrupeID);
            return View(model);
        }

        // POST: /Klientas/EditUserGroup/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUserGroup(EditUserViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Klientas klientas = _db.Klientai.Find(model.ID);
                    klientas.grupeID = model.GrupeID;
                    _db.Entry(klientas).State = EntityState.Modified;
                    _db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Įvyko klaida. Bandykite dar kartą.");
            }

            ViewBag.GrupesID = new SelectList(_db.Grupes.OrderBy(m => m.pavadinimas), "ID", "pavadinimas", model.GrupeID);
            return View(model);
        }

        // GET: /Klientas/EditUserRole/5
        [Authorize(Roles = "Administratorius")]
        public ActionResult EditUserRole(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Klientas klientas = _db.Klientai.Find(id);
            if (klientas == null)
            {
                return HttpNotFound();
            }

            EditUserViewModel model = new EditUserViewModel
            {
                ID = klientas.ID,
                teises = klientas.teises
            };

            ViewBag.TeisesList = new SelectList(new[]
            {
                new SelectListItem { Value = "Administratorius", Text = "Administratorius" },
                new SelectListItem { Value = "Buhalteris", Text = "Buhalteris" },
                new SelectListItem { Value = "Klientas", Text = "Klientas" }
            }, "Value", "Text", model.teises);
            return View(model);
        }

        // POST: /Klientas/EditUserGroup/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUserRole(EditUserViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Klientas klientas = _db.Klientai.Find(model.ID);
                    klientas.teises = model.teises;
                    _db.Entry(klientas).State = EntityState.Modified;
                    _db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Įvyko klaida. Bandykite dar kartą.");
            }

            ViewBag.Teises = new SelectList(new[]
            {
                new SelectListItem { Value = "Administratorius", Text = "Administratorius" },
                new SelectListItem { Value = "Buhalteris", Text = "Buhalteris" },
                new SelectListItem { Value = "Klientas", Text = "Klientas" }
            }, "Value", "Text", model.teises);
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
