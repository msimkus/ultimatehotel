﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Viesbutis.Models.MokejimasPosisteme;
using Viesbutis.DAL;
using System.Data.Entity.Infrastructure;

namespace Viesbutis.Controllers
{
    public class MokejimasController : Controller
    {
        private ViesbutisContext db = new ViesbutisContext();

        [Authorize(Roles = "Buhalteris")] 
        // GET: /Mokejimas/
        public ActionResult Index()
        {
            var mokejimai = db.Mokejimai.OrderBy(m => m.mokejimo_data);
            return View(mokejimai.ToList());
        }

        [Authorize(Roles = "Buhalteris")] 
        // GET: /Mokejimas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mokejimas mokejimas = db.Mokejimai.Find(id);
            if (mokejimas == null)
            {
                return HttpNotFound();
            }

            return View(mokejimas);
        }

        [Authorize(Roles = "Buhalteris")] 

        // GET: /Mokejimas/Create
        public ActionResult Create()
        {
            ViewBag.MokejimoTipai = new SelectList(db.MokejimoTipai.OrderBy(m => m.pavadinimas), "ID", "pavadinimas");
            ViewBag.MokejimoValiutos = new SelectList(db.MokejimoValiutos.OrderBy(m => m.pavadinimas), "ID", "pavadinimas");

            return View();
        }

        [Authorize(Roles = "Buhalteris")] 
        // POST: /Mokejimas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="klientas,mokejimotipasID,mokejimovaliutaID,mokejimo_data,mokejimo_suma,mokejimo_busena,kas_aptarnavo")] Mokejimas mokejimas)
        {
            if (ModelState.IsValid)
            {
                db.Mokejimai.Add(mokejimas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MokejimoTipai = new SelectList(db.MokejimoTipai.OrderBy(m => m.pavadinimas), "ID", "pavadinimas");
            ViewBag.MokejimoValiutos = new SelectList(db.MokejimoValiutos.OrderBy(m => m.pavadinimas), "ID", "pavadinimas");

            return View(mokejimas);
        }
        //_____________________________________________________

        [Authorize(Roles = "Buhalteris")] 
        // GET: /Mokejimas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mokejimas mokejimas = db.Mokejimai.Find(id);
            if (mokejimas == null)
            {
                return HttpNotFound();
            }

            ViewBag.MokejimoTipai = new SelectList(db.MokejimoTipai.OrderBy(m => m.pavadinimas), "ID", "pavadinimas", mokejimas.mokejimotipasID);
            ViewBag.MokejimoValiutos = new SelectList(db.MokejimoValiutos.OrderBy(m => m.pavadinimas), "ID", "pavadinimas", mokejimas.mokejimovaliutaID);

            return View(mokejimas);
        }

        [Authorize(Roles = "Buhalteris")] 
        // POST: /Mokejimas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,klientas,mokejimotipasID,mokejimovaliutaID,mokejimo_data,mokejimo_suma,mokejimo_busena,kas_aptarnavo")] Mokejimas mokejimas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mokejimas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MokejimoTipai = new SelectList(db.MokejimoTipai.OrderBy(m => m.pavadinimas), "ID", "pavadinimas", mokejimas.mokejimotipasID);
            ViewBag.MokejimoValiutos = new SelectList(db.MokejimoValiutos.OrderBy(m => m.pavadinimas), "ID", "pavadinimas", mokejimas.mokejimovaliutaID);

            return View(mokejimas);
        }

        [Authorize(Roles = "Buhalteris")] 
        // GET: /Mokejimas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mokejimas mokejimas = db.Mokejimai.Find(id);
            if (mokejimas == null)
            {
                return HttpNotFound();
            }
            return View(mokejimas);
        }

        [Authorize(Roles = "Buhalteris")] 
        // POST: /Mokejimas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Mokejimas mokejimas = db.Mokejimai.Find(id);
            db.Mokejimai.Remove(mokejimas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //____________________________________________________________________________
        //ataskaita vieno mokejimo
        public ActionResult DownloadActionAsPDF1(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mokejimas mokejimas = db.Mokejimai.Find(id);
            if (mokejimas == null)
            {
                return HttpNotFound();
            }

            Ataskaita ataskaita = new Ataskaita 
            {
                klientas = mokejimas.klientas,
                mokejimo_data = mokejimas.mokejimo_data,
                mokejimo_suma = mokejimas.mokejimo_suma,
                mokejimo_busena = mokejimas.mokejimo_busena,
                kas_aptarnavo = mokejimas.kas_aptarnavo,
                mokejimo_valiuta = mokejimas.MokejimoValiuta.pavadinimas,
                mokejimo_tipas = mokejimas.MokejimoTipas.pavadinimas


            };

            //var model = new GeneratePDFModel();
            //Code to get content
            return new Rotativa.ActionAsPdf("GeneratePDF1", ataskaita) { };
        }

        public ActionResult GeneratePDF1(Ataskaita ataskaita)
        {
            //var model = new GeneratePDFModel();
            //get content
            return View(ataskaita);
        }

        //ataskaita bendra vieno zmogaus _________________________________
        public ActionResult DownloadActionAsPDF2()
        {
            return new Rotativa.ActionAsPdf("GeneratePDF2") { };
        }

        public ActionResult GeneratePDF2()
        {
            int userID = db.Klientai.Where(model => model.prisijungimo_vardas == User.Identity.Name).FirstOrDefault().ID;
            var rezervacijos = db.Rezervacijos.Where(model => model.klientasID == userID);

            List<Ataskaita> mokejimuAtaskaita = new List<Ataskaita>();
            Ataskaita ataskaita;

            foreach (var rezervacija in rezervacijos)
            {
                if (rezervacija.Mokejimas != null)
                {
                    ataskaita = new Ataskaita
                    {
                        klientas = rezervacija.Mokejimas.klientas,
                        mokejimo_data = rezervacija.Mokejimas.mokejimo_data,
                        mokejimo_suma = rezervacija.Mokejimas.mokejimo_suma,
                        mokejimo_busena = rezervacija.Mokejimas.mokejimo_busena,
                        kas_aptarnavo = rezervacija.Mokejimas.kas_aptarnavo,
                        mokejimo_valiuta = rezervacija.Mokejimas.MokejimoValiuta.pavadinimas,
                        mokejimo_tipas = rezervacija.Mokejimas.MokejimoTipas.pavadinimas
                    };

                    mokejimuAtaskaita.Add(ataskaita);
                }
            }

            return View(mokejimuAtaskaita);
        }

        //ataskaita bendra _________________________________
        public ActionResult DownloadActionAsPDF3()
        {
            return new Rotativa.ActionAsPdf("GeneratePDF3") { };
        }

        public ActionResult GeneratePDF3()
        {
            var mokejimai = db.Mokejimai.OrderBy(model => model.mokejimo_data);

            if (mokejimai.Count() != 0)
            {
                List<Ataskaita> mokejimuAtaskaita = new List<Ataskaita>();
                Ataskaita ataskaita;

                foreach (var mokejimas in mokejimai)
                {
                    ataskaita = new Ataskaita
                    {
                        klientas = mokejimas.klientas,
                        mokejimo_data = mokejimas.mokejimo_data,
                        mokejimo_suma = mokejimas.mokejimo_suma,
                        mokejimo_busena = mokejimas.mokejimo_busena,
                        kas_aptarnavo = mokejimas.kas_aptarnavo,
                        mokejimo_valiuta = mokejimas.MokejimoValiuta.pavadinimas,
                        mokejimo_tipas = mokejimas.MokejimoTipas.pavadinimas
                    };

                    mokejimuAtaskaita.Add(ataskaita);
                }

                return View(mokejimuAtaskaita);
            }

            return RedirectToAction("Index");
        }
        //____________________________________________________________________________


        [Authorize(Roles = "Buhalteris")] 
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
