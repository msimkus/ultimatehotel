﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Viesbutis.DAL;
using System.Net;
using Viesbutis.Models.KlientasPosisteme;
using System.Data;
using System.Data.Entity;
using Viesbutis.Models.KlientasPosisteme.ViewModels;

namespace Viesbutis.Controllers
{
    public class MiestasController : Controller
    {
        private ViesbutisContext _db = new ViesbutisContext();

        // returns the list of cities
        [Authorize(Roles = "Administratorius")]
        public ActionResult Index()
        {
            var miestai = _db.Miestai.OrderBy(x => x.pavadinimas);

            return View(miestai);
        }

        // returns the details of single city
        [Authorize(Roles = "Administratorius")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Miestas miestas = _db.Miestai.Find(id);
            if (miestas == null)
            {
                return HttpNotFound();
            }

            MiestasViewModel miestasViewModel = new MiestasViewModel() 
            {
                ID = miestas.ID,
                pavadinimas = miestas.pavadinimas,
                kodas = miestas.kodas
            };

            return View(miestasViewModel);
        }

        [Authorize(Roles = "Administratorius")]
        public ActionResult Create()
        {
            return View();
        }

        // creates a new city
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pavadinimas, kodas")]MiestasViewModel miestasViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Miestas miestas = new Miestas() 
                    {
                        pavadinimas = miestasViewModel.pavadinimas,
                        kodas = miestasViewModel.kodas
                    };
                    _db.Miestai.Add(miestas);
                    _db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Įvyko klaida. Bandykite dar kartą.");
            }

            return View(miestasViewModel);
        }

        [Authorize(Roles = "Administratorius")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Miestas miestas = _db.Miestai.Find(id);
            if (miestas == null)
            {
                return HttpNotFound();
            }

            MiestasViewModel miestasViewModel = new MiestasViewModel() 
            {
                ID = miestas.ID,
                pavadinimas = miestas.pavadinimas,
                kodas = miestas.kodas
            };

            return View(miestasViewModel);
        }

        // edits the city information
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID, pavadinimas, kodas")]MiestasViewModel miestasViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Miestas miestas = new Miestas() 
                    {
                        ID = miestasViewModel.ID,
                        pavadinimas = miestasViewModel.pavadinimas,
                        kodas = miestasViewModel.kodas
                    };
                    _db.Entry(miestas).State = EntityState.Modified;
                    _db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Įvyko klaida. Bandykite dar kartą.");
            }

            return View(miestasViewModel);
        }

        [Authorize(Roles = "Administratorius")]
        public ActionResult Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Įvyko klaida. Bandykite dar kartą.";
            }

            Miestas miestas = _db.Miestai.Find(id);
            if (miestas == null)
            {
                return HttpNotFound();
            }

            MiestasViewModel miestasViewModel = new MiestasViewModel() 
            {
                ID = miestas.ID,
                pavadinimas = miestas.pavadinimas,
                kodas = miestas.kodas
            };

            return View(miestasViewModel);
        }

        // deletes the city
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Miestas miestas = _db.Miestai.Find(id);
                if (miestas.Klientai.Count() == 0)
                {
                    _db.Entry(miestas).State = EntityState.Deleted;
                    _db.SaveChanges();
                }
                else 
                {
                    ViewBag.ErrorMessage = "Miesto negalima ištrinti. Egzistuoja su juo susijusių vartotojų";
                    MiestasViewModel model = new MiestasViewModel 
                    {
                        ID = miestas.ID,
                        pavadinimas = miestas.pavadinimas,
                        kodas = miestas.kodas
                    };
                    return View(model);
                }
            }
            catch (DataException)
            {
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}