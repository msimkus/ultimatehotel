﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Viesbutis.DAL;
using Viesbutis.Models.RezervacijaPosisteme;

namespace Viesbutis.Controllers
{
    public class PaslaugasController : Controller
    {
        private ViesbutisContext db = new ViesbutisContext();

        // GET: Paslaugas
        public ActionResult Index()
        {
            return View(db.Paslaugos.ToList());
        }

        // GET: Paslaugas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paslauga paslauga = db.Paslaugos.Find(id);
            if (paslauga == null)
            {
                return HttpNotFound();
            }
            return View(paslauga);
        }

        // GET: Paslaugas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Paslaugas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,pavadinimas,aprasymas,kaina")] Paslauga paslauga)
        {
            if (ModelState.IsValid)
            {
                db.Paslaugos.Add(paslauga);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(paslauga);
        }

        // GET: Paslaugas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paslauga paslauga = db.Paslaugos.Find(id);
            if (paslauga == null)
            {
                return HttpNotFound();
            }
            return View(paslauga);
        }

        // POST: Paslaugas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,pavadinimas,aprasymas,kaina")] Paslauga paslauga)
        {
            if (ModelState.IsValid)
            {
                db.Entry(paslauga).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(paslauga);
        }

        // GET: Paslaugas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paslauga paslauga = db.Paslaugos.Find(id);
            if (paslauga == null)
            {
                return HttpNotFound();
            }
            return View(paslauga);
        }

        // POST: Paslaugas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Paslauga paslauga = db.Paslaugos.Find(id);
            db.Paslaugos.Remove(paslauga);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
