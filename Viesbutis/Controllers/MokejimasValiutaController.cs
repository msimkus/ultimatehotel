﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Viesbutis.DAL;
using Viesbutis.Models.MokejimasPosisteme;

namespace Viesbutis.Controllers
{
    public class MokejimasValiutaController : Controller
    {
        private ViesbutisContext db = new ViesbutisContext();
        [Authorize(Roles = "Buhalteris")] 
        // GET: /Mokejimas/
        public ActionResult Index()
        {
            var mokejimoValiuta = db.MokejimoValiutos.OrderBy(m => m.pavadinimas);
            return View(mokejimoValiuta.ToList());
        }

        [Authorize(Roles = "Buhalteris")] 
        // GET: /Mokejimas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MokejimoValiuta mokejimoValiuta = db.MokejimoValiutos.Find(id);
            if (mokejimoValiuta == null)
            {
                return HttpNotFound();
            }

            return View(mokejimoValiuta);
        }

        [Authorize(Roles = "Buhalteris")] 
        // GET: /Mokejimas/Create
        public ActionResult Create()
        {
            return View();
        }

        [Authorize(Roles = "Buhalteris")] 
        // POST: /mokejimoValiuta/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pavadinimas,kursas")] MokejimoValiuta mokejimoValiuta)
        {
            if (ModelState.IsValid)
            {
                db.MokejimoValiutos.Add(mokejimoValiuta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mokejimoValiuta);
        }

        [Authorize(Roles = "Buhalteris")] 
        // GET: /MokejimasValiutos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MokejimoValiuta mokejimoValiuta = db.MokejimoValiutos.Find(id);
            if (mokejimoValiuta == null)
            {
                return HttpNotFound();
            }

            return View(mokejimoValiuta);
        }

        [Authorize(Roles = "Buhalteris")] 
        // POST: /Mokejimas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,pavadinimas")] MokejimoValiuta mokejimoValiuta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mokejimoValiuta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mokejimoValiuta);
        }

        [Authorize(Roles = "Buhalteris")] 
        // GET: /Mokejimas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MokejimoValiuta mokejimoValiuta = db.MokejimoValiutos.Find(id);
            if (mokejimoValiuta == null)
            {
                return HttpNotFound();
            }
            return View(mokejimoValiuta);
        }

        [Authorize(Roles = "Buhalteris")] 
        // POST: /Mokejimas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MokejimoValiuta mokejimoValiuta = db.MokejimoValiutos.Find(id);
            db.MokejimoValiutos.Remove(mokejimoValiuta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Buhalteris")] 
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
	}
}