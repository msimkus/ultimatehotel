﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Viesbutis.Models.KambarysPosisteme;
using Viesbutis.DAL;

namespace Viesbutis.Models
{
    public class PatogumasController : Controller
    {
        private ViesbutisContext db = new ViesbutisContext();

       [Authorize(Roles = "Administratorius")]
        public ActionResult Index()
        {
            return View(db.Patogumai.ToList());
        }

        [Authorize(Roles = "Administratorius")]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize(Roles = "Administratorius")]
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Create([Bind(Include="ID,pavadinimas")] Patogumas patogumas)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    bool pasikartojapav = db.Patogumai.Where(model => model.pavadinimas == patogumas.pavadinimas).Any();
                    if (pasikartojapav)
                    {
                        ViewBag.ErrorMessage = "Patogumas su tokiu pavadinimujau egzistuoja.";
                    }
                    else
                    {
                        db.Patogumai.Add(patogumas);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (DataException )
            {
                ModelState.AddModelError("", "Neįmanoma išsaugoti pakeitimų. Pamėginkite dar kartą ir jei nepavyks susisiekite sus sistemos administratoriumi.");
            }

            return View(patogumas);
        }

        [Authorize(Roles = "Administratorius")]
        public ActionResult Edit(int? id)
        {
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Patogumas patogumas = db.Patogumai.Find(id);
            if (patogumas == null)
            {
                return HttpNotFound();
            }
            return View(patogumas);
        }

        [Authorize(Roles = "Administratorius")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,pavadinimas")] Patogumas patogumas)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(patogumas).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Neįmanoma išsaugoti pakeitimų. Pamėginkite dar kartą ir jei nepavyks susisiekite sus sistemos administratoriumi.");
            }
            return View(patogumas);
        }

        [Authorize(Roles = "Administratorius")]
        public ActionResult Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Ištrinti nepavyko.Try again, and if the problem persists see your system administrator.";
            }
            Patogumas patogumas = db.Patogumai.Find(id);
            if (patogumas == null)
            {
                return HttpNotFound();
            }
            return View(patogumas);
        }

        [Authorize(Roles = "Administratorius")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                Patogumas patogumas = db.Patogumai.Find(id);
                bool panaudotas = db.PatogumaiTarpines.Where(model => model.patogumasID== id).Any();
                if (panaudotas)
                {
                    ViewBag.ErrorMessage = "Yra kambarys su tokiu patogumu.";
                    return View(patogumas);
                }
                db.Patogumai.Remove(patogumas);
                db.SaveChanges();
                
            }
            catch (DataException)
            {
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administratorius")]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
