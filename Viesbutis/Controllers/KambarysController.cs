﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net; 
using System.Web;
using System.Web.Mvc;
using Viesbutis.Models.KambarysPosisteme;
using Viesbutis.DAL; 

namespace Viesbutis.Controllers
{
    public class KambarysController : Controller
    {
        private ViesbutisContext db = new ViesbutisContext();

        // GET: /Kambarys/
        public ActionResult Index()
        {
            var kambariai = db.Kambariai.Include(k => k.ApgyvendinimoTipas).Include(k => k.KambarioTipas);
            return View(kambariai.ToList());
        }

        // GET: /Kambarys/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kambarys kambarys = db.Kambariai.Find(id);
            if (kambarys == null)
            {
                return HttpNotFound();
            }
            return View(kambarys);
        }

        // GET: /Kambarys/Create
        [Authorize(Roles = "Administratorius")]
        public ActionResult Create()
        {
            ViewBag.apgyvendinimoTipasID = new SelectList(db.ApgyvendinimoTipai, "ID", "pavadinimas");
            ViewBag.kambarioTipasID = new SelectList(db.KambarioTipai, "ID", "trumpinys");
            return View();
        }


        [Authorize(Roles = "Administratorius")]
        // creates a new room
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,apgyvendinimoTipasID,kambarioTipasID,numeris,aukstas,plotas,kaina")] Kambarys kambarys)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    bool pasikartojaNr = db.Kambariai.Where(model => model.numeris == kambarys.numeris).Any();
                    if (pasikartojaNr)
                    {
                        ViewBag.ErrorMessage = "Kambarys su tokiu numeriu jau egzistuoja.";
                    }
                    else
                    {
                        db.Kambariai.Add(kambarys);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Įvyko klaida. Bandykite dar kartą.");
            }
            ViewBag.apgyvendinimoTipasID = new SelectList(db.ApgyvendinimoTipai, "ID", "pavadinimas", kambarys.apgyvendinimoTipasID);
            ViewBag.kambarioTipasID = new SelectList(db.KambarioTipai, "ID", "trumpinys", kambarys.kambarioTipasID);
            return View(kambarys);


        }

        [Authorize(Roles = "Administratorius")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kambarys kambarys = db.Kambariai.Find(id);
            if (kambarys == null)
            {
                return HttpNotFound();
            }
            ViewBag.apgyvendinimoTipasID = new SelectList(db.ApgyvendinimoTipai, "ID", "pavadinimas", kambarys.apgyvendinimoTipasID);
            ViewBag.kambarioTipasID = new SelectList(db.KambarioTipai, "ID", "trumpinys", kambarys.kambarioTipasID);
            return View(kambarys);
        }


        [Authorize(Roles = "Administratorius")]
        // edits information about room
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,apgyvendinimoTipasID,kambarioTipasID,numeris,aukstas,plotas,kaina")] Kambarys kambarys)
        {
            try
            {
                if (ModelState.IsValid)
                {
                   
                    bool rezervuotas = db.Rezervacijos.Where(model => model.kambarysID == kambarys.ID).Any();
                    if (rezervuotas)
                    {
                        ViewBag.ErrorMessage = "Jūs negalite redaguoti informacijos apie kambari. Jis rezervuotas.";
                        //return RedirectToAction("Edit");
                    }
                    else
                    {
                        db.Entry(kambarys).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    
                }
            }

            catch (DataException)
            {
                ModelState.AddModelError("", "Įvyko klaida. Bandykite dar kartą..");
            }
            ViewBag.apgyvendinimoTipasID = new SelectList(db.ApgyvendinimoTipai, "ID", "pavadinimas", kambarys.apgyvendinimoTipasID);
            ViewBag.kambarioTipasID = new SelectList(db.KambarioTipai, "ID", "trumpinys", kambarys.kambarioTipasID);
            return View(kambarys);
        }


        [Authorize(Roles = "Administratorius")]
        // GET: /Kambarys/Delete/5
        public ActionResult Delete(int? id, bool? saveChangesError = false)
        {
          
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Kambarys rezervuotas. Pašalinti jo negalima.";
            }
            Kambarys kambarys = db.Kambariai.Find(id);
            if (kambarys == null)
            {
                return HttpNotFound();
            }
            return View(kambarys);
        }

        [Authorize(Roles = "Administratorius")]
        // delete the romm
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                Kambarys kambarys = db.Kambariai.Find(id);
                bool rezervuotas = db.RezervacijosLaikai.Where(model => model.kambarysID == id && model.uzimta == true).Any();
                if (rezervuotas)
                {
                    ViewBag.ErrorMessage = "Kambarys rezervuotas. Jo pašalinti negalima.";
                    return View(kambarys);
                }
                db.Kambariai.Remove(kambarys);
                db.SaveChanges();
            }
            catch (DataException)
            {
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administratorius")]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
