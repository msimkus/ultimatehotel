﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Viesbutis.Models.RezervacijaPosisteme;
using Viesbutis.DAL;
using System.Data.Entity.Infrastructure;

namespace Viesbutis.Controllers
{
    public class RezervacijaController : Controller
    {
        private ViesbutisContext db = new ViesbutisContext();

        // GET: /Rezervacija/
        public ActionResult Index()
        {
            //var rezervacijos = db.Rezervacijos.Include(r => r.Kambarys).Include(r => r.Klientas).Include(r => r.Mokejimas).Include(r => r.RezervacijosLaikas);
            //return View(rezervacijos.ToList());
            string teises = db.Klientai.Where(model => model.prisijungimo_vardas == User.Identity.Name).FirstOrDefault().teises;
            int id = db.Klientai.Where(model => model.prisijungimo_vardas == User.Identity.Name).FirstOrDefault().ID;
            var rezervacijos = db.Rezervacijos.Include(r => r.Kambarys).Include(r => r.Klientas).Include(r => r.Mokejimas).Include(r => r.RezervacijosLaikas);
            if (teises.Equals("Klientas")) return View(rezervacijos.ToList().Where(s => s.klientasID == id));
            else return View(rezervacijos.ToList());
        }

        // GET: /Rezervacija/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rezervacija rezervacija = db.Rezervacijos.Find(id);
            if (rezervacija == null)
            {
                return HttpNotFound();
            }
            return View(rezervacija);
        }

        // GET: /Rezervacija/Create
        // KEICIAU ----- gAUNA ID - KAMBARIO, KURIS REZERVUOJAMAS ID
        public ActionResult Create(int id)
        {
            //-------KEICIAU-------------
            ViewBag.kambarysID = id;

            //---------KEICIAU-------------------------
            // iskart pasiima userID, kuris rezervuojasi
            int userID = db.Klientai.Where(model => model.prisijungimo_vardas == User.Identity.Name).FirstOrDefault().ID;
            ViewBag.klientasID = userID;
            //ViewBag.klientasID = new SelectList(db.Klientai, "ID", "vardas");
            //-----------------------------------------

            //--------KEICIAU------------------------------
            // Duoda pasirinkt tik is tu laiku, kurie susieti su kambariu, kurio id perduodamas ir kurie nera uzimti;    
            ViewBag.rezervacijoslaikasID = new SelectList(db.RezervacijosLaikai.Where(model => model.kambarysID == id && model.uzimta == false), "ID", "rezervuota_nuo_iki");
            //ViewBag.rezervacijoslaikasID = new SelectList(db.RezervacijosLaikai, "ID", "ID");
            //----------------------------------------------

            return View();
        }

        // POST: /Rezervacija/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "klientasID,kambarysID,rezervacijoslaikasID,patvirtinta")] Rezervacija rezervacija)
        {
            if (ModelState.IsValid)
            {

                List<Viesbutis.Models.MokejimasPosisteme.Mokejimas> Mokejimai = db.Mokejimai.ToList();
                List<Viesbutis.Models.KambarysPosisteme.Kambarys> Kambariai = db.Kambariai.ToList();
                List<Viesbutis.Models.MokejimasPosisteme.MokejimoTipas> MokTipas = db.MokejimoTipai.ToList();
                List<Viesbutis.Models.MokejimasPosisteme.MokejimoValiuta> MokValiuta = db.MokejimoValiutos.ToList();

                Viesbutis.Models.MokejimasPosisteme.Mokejimas mokejimas = new Models.MokejimasPosisteme.Mokejimas();
                //if (Mokejimai.Count == 0) mokejimas.ID = 1;
                //else mokejimas.ID = Mokejimai[Mokejimai.Count - 1].ID + 1;
                mokejimas.kas_aptarnavo = "Automatinė sistema";

                int userID = db.Klientai.Where(model => model.prisijungimo_vardas == User.Identity.Name).FirstOrDefault().ID;
                mokejimas.klientas = db.Klientai.Find(userID).vardas + " " + db.Klientai.Find(userID).pavarde;

                mokejimas.mokejimo_busena = false;
                mokejimas.mokejimo_data = DateTime.Now;
                for (int i = 0; i < Kambariai.Count; i++)
                    if (Kambariai[i].ID == rezervacija.kambarysID)
                        mokejimas.mokejimo_suma = Kambariai[i].kaina;
                mokejimas.MokejimoTipas = MokTipas[0];
                mokejimas.mokejimotipasID = MokTipas[0].ID;
                mokejimas.MokejimoValiuta = MokValiuta[0];
                mokejimas.mokejimovaliutaID = MokValiuta[0].ID;
                db.Mokejimai.Add(mokejimas);
                db.SaveChanges();

                rezervacija.mokejimasID = mokejimas.ID;
                db.Rezervacijos.Add(rezervacija);

                RezervacijosLaikas rezlaikas = db.RezervacijosLaikai.Find(rezervacija.rezervacijoslaikasID);
                rezlaikas.uzimta = true;
                db.Entry(rezlaikas).State = EntityState.Modified;

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.kambarysID = rezervacija.kambarysID;
            ViewBag.klientasID = new SelectList(db.Klientai, "ID", "vardas", rezervacija.klientasID);
            ViewBag.mokejimasID = rezervacija.mokejimasID;//new SelectList(db.Mokejimai, "ID", "ID", rezervacija.mokejimasID);
            ViewBag.rezervacijoslaikasID = new SelectList(db.RezervacijosLaikai, "ID", "rezervuota_nuo_iki", rezervacija.rezervacijoslaikasID);
            return View(rezervacija);
        }

        [Authorize(Roles = "Administratorius")]
        // GET: /Rezervacija/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rezervacija rezervacija = db.Rezervacijos.Find(id);
            if (rezervacija == null)
            {
                return HttpNotFound();
            }
            //ViewBag.kambarysID = new SelectList(db.Kambariai, "ID", "ID", rezervacija.kambarysID);
            //ViewBag.klientasID = new SelectList(db.Klientai, "ID", "vardas", rezervacija.klientasID);
            //ViewBag.mokejimasID = new SelectList(db.Mokejimai, "ID", "ID", rezervacija.mokejimasID);
            //ViewBag.rezervacijoslaikasID = new SelectList(db.RezervacijosLaikai, "ID", "rezervuota_nuo_iki", rezervacija.rezervacijoslaikasID);
            return View(rezervacija);
        }

        // POST: /Rezervacija/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Administratorius")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,patvirtinta")] Rezervacija rezervacija)
        {
            if (ModelState.IsValid)
            {
                var rez = db.Rezervacijos.Find(rezervacija.ID);
                rez.patvirtinta = rezervacija.patvirtinta;

                db.Entry(rez).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.kambarysID = new SelectList(db.Kambariai, "ID", "ID", rezervacija.kambarysID);
            //ViewBag.klientasID = new SelectList(db.Klientai, "ID", "vardas", rezervacija.klientasID);
            //ViewBag.mokejimasID = new SelectList(db.Mokejimai, "ID", "ID", rezervacija.mokejimasID);
            //ViewBag.rezervacijoslaikasID = new SelectList(db.RezervacijosLaikai, "ID", "rezervuota_nuo_iki", rezervacija.rezervacijoslaikasID);
            return View(rezervacija);
        }

        // GET: /Rezervacija/Delete/5
        [Authorize(Roles = "Administratorius,Klientas")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rezervacija rezervacija = db.Rezervacijos.Find(id);
            if (rezervacija == null)
            {
                return HttpNotFound();
            }
            return View(rezervacija);
        }

        [Authorize(Roles = "Administratorius,Klientas")]
        // POST: /Rezervacija/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Rezervacija rezervacija = db.Rezervacijos.Find(id);
            db.Rezervacijos.Remove(rezervacija);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administratorius,Klientas")]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}