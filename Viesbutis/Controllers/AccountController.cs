﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Viesbutis.Models;
using Viesbutis.DAL;
using System.Web.Security;
using Viesbutis.Models.KlientasPosisteme.ViewModels;
using Viesbutis.Models.KlientasPosisteme;
using System.Data;
using System.Data.Entity;
using System.Security.Cryptography;
using System.Text;

namespace Viesbutis.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ViesbutisContext _db = new ViesbutisContext();

        public AccountController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

        public AccountController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }

        public UserManager<ApplicationUser> UserManager { get; private set; }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            // Lets first check if the Model is valid or not
            if (ModelState.IsValid)
            {
                using (ViesbutisContext _db = new ViesbutisContext())
                {
                    string username = model.UserName;
                    string password = model.Password;

                    string hashedPassword = CalculateMD5Hash(password);

                    bool userValid = _db.Klientai.Any(user => user.prisijungimo_vardas == username && user.slaptazodis == hashedPassword);

                    // User found in the database
                    if (userValid)
                    {
                        FormsAuthentication.SetAuthCookie(username, false);
                        if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                            && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                        {
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            if (_db.Klientai.Where(m => m.prisijungimo_vardas == model.UserName).FirstOrDefault().teises == "Administratorius")
                                return RedirectToAction("Index", "Rezervacija");
                            else if (_db.Klientai.Where(m => m.prisijungimo_vardas == model.UserName).FirstOrDefault().teises == "Buhalteris")
                                return RedirectToAction("Index", "Mokejimas");
                            else
                                return RedirectToAction("Index", "Kambarys");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Neteisingai nurodytas prisijungimo vardas arba slaptažodis.");
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            ViewBag.Lytys = new SelectList(new[]
            {
                new SelectListItem { Value = "M", Text = "M" },
                new SelectListItem { Value = "V", Text = "V" }
            }, "Value", "Text", "M");
            ViewBag.BankaiID = new SelectList(_db.Bankai.OrderBy(m => m.pavadinimas), "ID", "pavadinimas");
            ViewBag.MiestaiID = new SelectList(_db.Miestai.OrderBy(m => m.pavadinimas), "ID", "pavadinimas");
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (_db.Klientai.Where(x => x.prisijungimo_vardas == model.UserName).Count() == 0)
                    {
                        Grupe grupe = _db.Grupes.Where(x => x.pavadinimas == "Neaktyvus").FirstOrDefault();
                        if (grupe == null) 
                        {
                            grupe = new Grupe 
                            {
                                pavadinimas = "Neaktyvus",
                                nuolaida = 0,
                                aprasymas = "Neaktyvus klientas"
                            };
                            _db.Grupes.Add(grupe);
                            _db.SaveChanges();
                        }

                        Klientas klientas = new Klientas
                        {
                            vardas = model.vardas,
                            pavarde = model.pavarde,
                            lytis = model.lytis,
                            miestasID = model.MiestasID,
                            telefono_nr = model.telefono_nr,
                            el_pastas = model.el_pastas,
                            bankasID = model.BankasID,
                            korteles_nr = model.korteles_nr,
                            grupeID = grupe.ID,
                            prisijungimo_vardas = model.UserName,
                            slaptazodis = CalculateMD5Hash(model.Password),
                            teises = "Klientas"
                        };

                        _db.Klientai.Add(klientas);
                        _db.SaveChanges();

                        FormsAuthentication.SetAuthCookie(model.UserName, false);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Toks vartotojo prisijungimo vardas jau yra. Pasirinkite kitą");
                    }
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Įvyko klaida. Bandykite dar kartą.");
            }
            // If we got this far, something failed, redisplay form
            ViewBag.Lytys = new SelectList(new[]
            {
                new SelectListItem { Value = "M", Text = "M" },
                new SelectListItem { Value = "V", Text = "V" }
            }, "Value", "Text", model.lytis);
            ViewBag.BankaiID = new SelectList(_db.Bankai.OrderBy(m => m.pavadinimas), "ID", "pavadinimas", model.BankasID);
            ViewBag.MiestaiID = new SelectList(_db.Miestai.OrderBy(m => m.pavadinimas), "ID", "pavadinimas", model.MiestasID);

            return View(model);
        }

        ////
        //// POST: /Account/Disassociate
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Disassociate(string loginProvider, string providerKey)
        //{
        //    ManageMessageId? message = null;
        //    IdentityResult result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
        //    if (result.Succeeded)
        //    {
        //        message = ManageMessageId.RemoveLoginSuccess;
        //    }
        //    else
        //    {
        //        message = ManageMessageId.Error;
        //    }
        //    return RedirectToAction("Manage", new { Message = message });
        //}

        public ActionResult UserDetails()
        {
            var user = _db.Klientai.Where(model => model.prisijungimo_vardas == User.Identity.Name).FirstOrDefault();

            KlientasViewModel klientasViewModel = new KlientasViewModel 
            { 
                ID = user.ID,
                vardas = user.vardas,
                pavarde = user.pavarde,
                lytis = user.lytis,
                Miestas = user.Miestas,
                telefono_nr = user.telefono_nr,
                el_pastas = user.el_pastas,
                Bankas = user.Bankas,
                korteles_nr = user.korteles_nr,
                Grupe = user.Grupe
            };

            return View(klientasViewModel);
        }

        public ActionResult ChangePassword() 
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = _db.Klientai.Where(x => x.prisijungimo_vardas == User.Identity.Name).FirstOrDefault();
                    if (user.slaptazodis == CalculateMD5Hash(model.OldPassword)) 
                    {
                        Klientas klientas = _db.Klientai.Find(user.ID);
                        klientas.slaptazodis = CalculateMD5Hash(model.NewPassword);
                        _db.Entry(klientas).State = EntityState.Modified;
                        _db.SaveChanges();
                    }
                    else
                    {
                        ModelState.AddModelError("", "Neteisingai nurodėte senąjį slaptažodį.");
                    }
                    
                    return RedirectToAction("UserDetails");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Įvyko klaida. Bandykite dar kartą.");
            }

            return View(model);
        }

        public ActionResult ChangeUserDetails()
        {
            var user = _db.Klientai.Where(x => x.prisijungimo_vardas == User.Identity.Name).FirstOrDefault();

            EditUserDetailsViewModel model = new EditUserDetailsViewModel 
            {
                vardas = user.vardas,
                pavarde = user.pavarde,
                lytis = user.lytis,
                MiestasID = user.miestasID,
                telefono_nr = user.telefono_nr,
                el_pastas = user.el_pastas,
                BankasID = user.bankasID,
                korteles_nr = user.korteles_nr
            };

            ViewBag.Lytys = new SelectList(new[]
            {
                new SelectListItem { Value = "M", Text = "M" },
                new SelectListItem { Value = "V", Text = "V" }
            }, "Value", "Text", model.lytis);
            ViewBag.BankaiID = new SelectList(_db.Bankai.OrderBy(m => m.pavadinimas), "ID", "pavadinimas", model.BankasID);
            ViewBag.MiestaiID = new SelectList(_db.Miestai.OrderBy(m => m.pavadinimas), "ID", "pavadinimas", model.MiestasID);

            return View(model);
        }

        [HttpPost]
        public ActionResult ChangeUserDetails(EditUserDetailsViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = _db.Klientai.Where(x => x.prisijungimo_vardas == User.Identity.Name).FirstOrDefault();
                    user.vardas = model.vardas;
                    user.pavarde = model.pavarde;
                    user.lytis = model.lytis;
                    user.miestasID = model.MiestasID;
                    user.telefono_nr = model.telefono_nr;
                    user.el_pastas = model.el_pastas;
                    user.bankasID = model.BankasID;
                    user.korteles_nr = model.korteles_nr;

                    _db.Entry(user).State = EntityState.Modified;
                    _db.SaveChanges();

                    return RedirectToAction("UserDetails");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Įvyko klaida. Bandykite dar kartą.");
            }

            ViewBag.Lytys = new SelectList(new[]
            {
                new SelectListItem { Value = "M", Text = "M" },
                new SelectListItem { Value = "V", Text = "V" }
            }, "Value", "Text", model.lytis);
            ViewBag.BankaiID = new SelectList(_db.Bankai.OrderBy(m => m.pavadinimas), "ID", "pavadinimas", model.BankasID);
            ViewBag.MiestaiID = new SelectList(_db.Miestai.OrderBy(m => m.pavadinimas), "ID", "pavadinimas", model.MiestasID);

            return View(model);
        }

        ////
        //// POST: /Account/ExternalLogin
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public ActionResult ExternalLogin(string provider, string returnUrl)
        //{
        //    // Request a redirect to the external login provider
        //    return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        //}

        ////
        //// GET: /Account/ExternalLoginCallback
        //[AllowAnonymous]
        //public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        //{
        //    var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
        //    if (loginInfo == null)
        //    {
        //        return RedirectToAction("Login");
        //    }

        //    // Sign in the user with this external login provider if the user already has a login
        //    var user = await UserManager.FindAsync(loginInfo.Login);
        //    if (user != null)
        //    {
        //        await SignInAsync(user, isPersistent: false);
        //        return RedirectToLocal(returnUrl);
        //    }
        //    else
        //    {
        //        // If the user does not have an account, then prompt the user to create an account
        //        ViewBag.ReturnUrl = returnUrl;
        //        ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
        //        return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { UserName = loginInfo.DefaultUserName });
        //    }
        //}

        ////
        //// POST: /Account/LinkLogin
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult LinkLogin(string provider)
        //{
        //    // Request a redirect to the external login provider to link a login for the current user
        //    return new ChallengeResult(provider, Url.Action("LinkLoginCallback", "Account"), User.Identity.GetUserId());
        //}

        ////
        //// GET: /Account/LinkLoginCallback
        //public async Task<ActionResult> LinkLoginCallback()
        //{
        //    var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
        //    if (loginInfo == null)
        //    {
        //        return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
        //    }
        //    var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
        //    if (result.Succeeded)
        //    {
        //        return RedirectToAction("Manage");
        //    }
        //    return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
        //}

        ////
        //// POST: /Account/ExternalLoginConfirmation
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        //{
        //    if (User.Identity.IsAuthenticated)
        //    {
        //        return RedirectToAction("Manage");
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        // Get the information about the user from the external login provider
        //        var info = await AuthenticationManager.GetExternalLoginInfoAsync();
        //        if (info == null)
        //        {
        //            return View("ExternalLoginFailure");
        //        }
        //        var user = new ApplicationUser() { UserName = model.UserName };
        //        var result = await UserManager.CreateAsync(user);
        //        if (result.Succeeded)
        //        {
        //            result = await UserManager.AddLoginAsync(user.Id, info.Login);
        //            if (result.Succeeded)
        //            {
        //                await SignInAsync(user, isPersistent: false);
        //                return RedirectToLocal(returnUrl);
        //            }
        //        }
        //        AddErrors(result);
        //    }

        //    ViewBag.ReturnUrl = returnUrl;
        //    return View(model);
        //}

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        ////
        //// GET: /Account/ExternalLoginFailure
        //[AllowAnonymous]
        //public ActionResult ExternalLoginFailure()
        //{
        //    return View();
        //}

        //[ChildActionOnly]
        //public ActionResult RemoveAccountList()
        //{
        //    var linkedAccounts = UserManager.GetLogins(User.Identity.GetUserId());
        //    ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
        //    return (ActionResult)PartialView("_RemoveAccountPartial", linkedAccounts);
        //}

        public string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri) : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}