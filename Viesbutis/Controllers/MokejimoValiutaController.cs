﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Viesbutis.Models.MokejimasPosisteme;
using Viesbutis.DAL;

namespace Viesbutis.Controllers
{
    public class MokejimoValiutaController : Controller
    {
        private ViesbutisContext db = new ViesbutisContext();

        // GET: /MokejimoValiuta/
        public ActionResult Index()
        {
            return View(db.MokejimoValiutos.ToList());
        }

        // GET: /MokejimoValiuta/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MokejimoValiuta mokejimovaliuta = db.MokejimoValiutos.Find(id);
            if (mokejimovaliuta == null)
            {
                return HttpNotFound();
            }
            return View(mokejimovaliuta);
        }

        // GET: /MokejimoValiuta/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /MokejimoValiuta/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,pavadinimas,kursas")] MokejimoValiuta mokejimovaliuta)
        {
            if (ModelState.IsValid)
            {
                db.MokejimoValiutos.Add(mokejimovaliuta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mokejimovaliuta);
        }

        // GET: /MokejimoValiuta/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MokejimoValiuta mokejimovaliuta = db.MokejimoValiutos.Find(id);
            if (mokejimovaliuta == null)
            {
                return HttpNotFound();
            }
            return View(mokejimovaliuta);
        }

        // POST: /MokejimoValiuta/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,pavadinimas,kursas")] MokejimoValiuta mokejimovaliuta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mokejimovaliuta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mokejimovaliuta);
        }

        // GET: /MokejimoValiuta/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MokejimoValiuta mokejimovaliuta = db.MokejimoValiutos.Find(id);
            if (mokejimovaliuta == null)
            {
                return HttpNotFound();
            }
            return View(mokejimovaliuta);
        }

        // POST: /MokejimoValiuta/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MokejimoValiuta mokejimovaliuta = db.MokejimoValiutos.Find(id);
            db.MokejimoValiutos.Remove(mokejimovaliuta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
