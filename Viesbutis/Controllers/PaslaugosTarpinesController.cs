﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Viesbutis.DAL;
using Viesbutis.Models.RezervacijaPosisteme;

namespace Viesbutis.Controllers
{
    public class PaslaugosTarpinesController : Controller
    {
        private ViesbutisContext db = new ViesbutisContext();

        // GET: PaslaugosTarpines
        public ActionResult Index()
        {
            var paslaugosTarpines = db.PaslaugosTarpines.Include(p => p.Paslauga).Include(p => p.Rezervacija);
            return View(paslaugosTarpines.ToList());
        }

        // GET: PaslaugosTarpines/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PaslaugosTarpine paslaugosTarpine = db.PaslaugosTarpines.Find(id);
            if (paslaugosTarpine == null)
            {
                return HttpNotFound();
            }
            return View(paslaugosTarpine);
        }

        // GET: PaslaugosTarpines/Create
        public ActionResult Create()
        {
            ViewBag.paslaugaID = new SelectList(db.Paslaugos, "ID", "pavadinimas");
            ViewBag.rezervacijaID = new SelectList(db.Rezervacijos, "ID", "ID");
            return View();
        }

        // POST: PaslaugosTarpines/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,rezervacijaID,paslaugaID")] PaslaugosTarpine paslaugosTarpine)
        {
            if (ModelState.IsValid)
            {
                db.PaslaugosTarpines.Add(paslaugosTarpine);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.paslaugaID = new SelectList(db.Paslaugos, "ID", "pavadinimas", paslaugosTarpine.paslaugaID);
            ViewBag.rezervacijaID = new SelectList(db.Rezervacijos, "ID", "ID", paslaugosTarpine.rezervacijaID);
            return View(paslaugosTarpine);
        }

        // GET: PaslaugosTarpines/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PaslaugosTarpine paslaugosTarpine = db.PaslaugosTarpines.Find(id);
            if (paslaugosTarpine == null)
            {
                return HttpNotFound();
            }
            ViewBag.paslaugaID = new SelectList(db.Paslaugos, "ID", "pavadinimas", paslaugosTarpine.paslaugaID);
            ViewBag.rezervacijaID = new SelectList(db.Rezervacijos, "ID", "ID", paslaugosTarpine.rezervacijaID);
            return View(paslaugosTarpine);
        }

        // POST: PaslaugosTarpines/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,rezervacijaID,paslaugaID")] PaslaugosTarpine paslaugosTarpine)
        {
            if (ModelState.IsValid)
            {
                db.Entry(paslaugosTarpine).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.paslaugaID = new SelectList(db.Paslaugos, "ID", "pavadinimas", paslaugosTarpine.paslaugaID);
            ViewBag.rezervacijaID = new SelectList(db.Rezervacijos, "ID", "ID", paslaugosTarpine.rezervacijaID);
            return View(paslaugosTarpine);
        }

        // GET: PaslaugosTarpines/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PaslaugosTarpine paslaugosTarpine = db.PaslaugosTarpines.Find(id);
            if (paslaugosTarpine == null)
            {
                return HttpNotFound();
            }
            return View(paslaugosTarpine);
        }

        // POST: PaslaugosTarpines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PaslaugosTarpine paslaugosTarpine = db.PaslaugosTarpines.Find(id);
            db.PaslaugosTarpines.Remove(paslaugosTarpine);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
