﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Viesbutis.Models.KambarysPosisteme;
using Viesbutis.DAL;

namespace Viesbutis.Controllers
{
    public class IvertinimasController : Controller
    {
        private ViesbutisContext db = new ViesbutisContext();

        // GET: /Ivertinimas/
        public ActionResult Index()
        {
            var ivertinimai = db.Ivertinimai.Include(i => i.Kambarys);
            return View(ivertinimai.ToList());
        }


        [Authorize(Roles = "Klientas")]
        public ActionResult Create(int id)
        {
            ViewBag.kambarysID = id;
            // ViewBag.kambarysID = new SelectList(db.Kambariai, "ID", "numeris");
            return View();
        }

        [Authorize(Roles = "Klientas")]
        //create new ivertinima
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,kambarysID,vertinimas,komentaras")] Ivertinimas ivertinimas)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userID = db.Klientai.Where(model => model.prisijungimo_vardas == User.Identity.Name).FirstOrDefault().ID;
                    bool rezervuotas = db.Rezervacijos.Where(model => model.kambarysID == ivertinimas.kambarysID && model.klientasID == userID && model.patvirtinta == true ).Any();
                    if (!rezervuotas)
                    {
                        ViewBag.ErrorMessage = "Jūs negalite vertinti kambario, kurio nėra jūsų rezervavimų istorijoje arba kuris dar nepatvirtintas.";
                        return View(ivertinimas);
                    }
                    db.Ivertinimai.Add(ivertinimas);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Įvyko klaida. Bandykite dar kartą.");
            }

            ViewBag.kambarysID = ivertinimas.kambarysID;
            //ViewBag.kambarysID = new SelectList(db.Kambariai, "ID", "numeris", ivertinimas.kambarysID);
            return View(ivertinimas);
        }


         [Authorize(Roles = "Administratorius")]

        // GET: /Ivertinimas/Delete/5
        public ActionResult Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Įvyko klaida. Bandykite dar kartą.";
            }
            Ivertinimas ivertinimas = db.Ivertinimai.Find(id);
            if (ivertinimas == null)
            {
                return HttpNotFound();
            }
            return View(ivertinimas);
        }


         [Authorize(Roles = "Administratorius")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                Ivertinimas ivertinimas = db.Ivertinimai.Find(id);
                db.Ivertinimai.Remove(ivertinimas);
                db.SaveChanges();
            }
            catch (DataException)
            {
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("Index");
        }

         [Authorize(Roles = "Administratorius")]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
