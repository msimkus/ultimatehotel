﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Viesbutis.DAL;
using Viesbutis.Models.RezervacijaPosisteme;

namespace Viesbutis.Controllers
{
    public class RezervacijosLaikasController : Controller
    {
        private ViesbutisContext db = new ViesbutisContext();

        // GET: RezervacijosLaikas
        //[Authorize(Roles = "Administratorius")]
        [Authorize(Roles = "Administratorius")]
        public ActionResult Index()
        {
            return View(db.RezervacijosLaikai.ToList());
        }

        // GET: RezervacijosLaikas/Details/5
       //[Authorize(Roles = "Administratorius")]
        [Authorize(Roles = "Administratorius")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RezervacijosLaikas rezervacijosLaikas = db.RezervacijosLaikai.Find(id);
            if (rezervacijosLaikas == null)
            {
                return HttpNotFound();
            }
            return View(rezervacijosLaikas);
        }

        // GET: RezervacijosLaikas/Create
       //[Authorize(Roles = "Administratorius")]
        [Authorize(Roles = "Administratorius")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: RezervacijosLaikas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
       //[Authorize(Roles = "Administratorius")]
        [Authorize(Roles = "Administratorius")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,kambarysID,rezervuota_nuo,rezervuota_iki,uzimta")] RezervacijosLaikas rezervacijosLaikas)
        {
            if (ModelState.IsValid)
            {
                db.RezervacijosLaikai.Add(rezervacijosLaikas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rezervacijosLaikas);
        }

        // GET: RezervacijosLaikas/Edit/5
        //[Authorize(Roles = "Administratorius")]
        [Authorize(Roles = "Administratorius")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RezervacijosLaikas rezervacijosLaikas = db.RezervacijosLaikai.Find(id);
            if (rezervacijosLaikas == null)
            {
                return HttpNotFound();
            }
            return View(rezervacijosLaikas);
        }

        // POST: RezervacijosLaikas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
       //[Authorize(Roles = "Administratorius")]
        [Authorize(Roles = "Administratorius")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,kambarysID,rezervuota_nuo,rezervuota_iki,uzimta")] RezervacijosLaikas rezervacijosLaikas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rezervacijosLaikas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(rezervacijosLaikas);
        }

        // GET: RezervacijosLaikas/Delete/5
      // [Authorize(Roles = "Administratorius")]
        [Authorize(Roles = "Administratorius")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RezervacijosLaikas rezervacijosLaikas = db.RezervacijosLaikai.Find(id);
            if (rezervacijosLaikas == null)
            {
                return HttpNotFound();
            }
            return View(rezervacijosLaikas);
        }

        // POST: RezervacijosLaikas/Delete/5
    //    [Authorize(Roles = "Administratorius")]
        [Authorize(Roles = "Administratorius")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RezervacijosLaikas rezervacijosLaikas = db.RezervacijosLaikai.Find(id);
            db.RezervacijosLaikai.Remove(rezervacijosLaikas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

  //      [Authorize(Roles = "Administratorius")]
        [Authorize(Roles = "Administratorius")]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
