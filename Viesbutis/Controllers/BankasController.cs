﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Viesbutis.DAL;
using System.Net;
using Viesbutis.Models.KlientasPosisteme;
using System.Data;
using System.Data.Entity;
using Viesbutis.Models.KlientasPosisteme.ViewModels;

namespace Viesbutis.Controllers
{
    public class BankasController : Controller
    {
        private ViesbutisContext _db = new ViesbutisContext();

        // returns the list of banks
        [Authorize(Roles = "Administratorius")]
        public ActionResult Index()
        {
            var bankai = _db.Bankai.OrderBy(x => x.pavadinimas);

            return View(bankai);
        }

        // returns the details of single bank
        [Authorize(Roles = "Administratorius")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Bankas bankas = _db.Bankai.Find(id);
            if (bankas == null)
            {
                return HttpNotFound();
            }

            BankasViewModel bankasViewModel = new BankasViewModel() 
            {
                ID = bankas.ID,
                pavadinimas = bankas.pavadinimas,
                kodas = bankas.kodas,
                adresas = bankas.adresas
            };

            return View(bankasViewModel);
        }

        [Authorize(Roles = "Administratorius")]
        public ActionResult Create()
        {
            return View();
        }

        // creates a new bank
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pavadinimas, adresas, kodas")]BankasViewModel bankasViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Bankas bankas = new Bankas() 
                    {
                        pavadinimas = bankasViewModel.pavadinimas,
                        kodas = bankasViewModel.kodas,
                        adresas = bankasViewModel.adresas
                    };
                    _db.Bankai.Add(bankas);
                    _db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Įvyko klaida. Bandykite dar kartą.");
            }

            return View(bankasViewModel);
        }

        [Authorize(Roles = "Administratorius")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Bankas bankas = _db.Bankai.Find(id);
            if (bankas == null)
            {
                return HttpNotFound();
            }

            BankasViewModel bankasViewModel = new BankasViewModel() 
            {
                ID = bankas.ID,
                pavadinimas = bankas.pavadinimas,
                kodas = bankas.kodas,
                adresas = bankas.adresas
            };

            return View(bankasViewModel);
        }

        // edits the bank information
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID, pavadinimas, adresas, kodas")]BankasViewModel bankasViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Bankas bankas = new Bankas() 
                    {
                        ID = bankasViewModel.ID,
                        pavadinimas = bankasViewModel.pavadinimas,
                        kodas = bankasViewModel.kodas,
                        adresas = bankasViewModel.adresas
                    };
                    _db.Entry(bankas).State = EntityState.Modified;
                    _db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Įvyko klaida. Bandykite dar kartą.");
            }

            return View(bankasViewModel);
        }

        [Authorize(Roles = "Administratorius")]
        public ActionResult Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Įvyko klaida. Bandykite dar kartą.";
            }

            Bankas bankas = _db.Bankai.Find(id);
            if (bankas == null)
            {
                return HttpNotFound();
            }

            BankasViewModel bankasViewModel = new BankasViewModel() 
            {
                ID = bankas.ID,
                pavadinimas = bankas.pavadinimas,
                kodas = bankas.kodas,
                adresas = bankas.adresas
            };

            return View(bankasViewModel);
        }

        // deletes the bank
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Bankas bankas = _db.Bankai.Find(id);
                if (bankas.Klientai.Count() == 0)
                {
                    _db.Entry(bankas).State = EntityState.Deleted;
                    _db.SaveChanges();
                }
                else
                {
                    ViewBag.ErrorMessage = "Banko negalima ištrinti. Egzistuoja su juo susijusių vartotojų";
                    BankasViewModel model = new BankasViewModel
                    {
                        ID = bankas.ID,
                        pavadinimas = bankas.pavadinimas,
                        kodas = bankas.kodas
                    };
                    return View(model);
                }
            }
            catch (DataException)
            {
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}