﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Viesbutis.Models.KambarysPosisteme;
using Viesbutis.DAL;

namespace Viesbutis.Controllers
{
    public class PatogumaiTarpineController : Controller
    {
        private ViesbutisContext db = new ViesbutisContext();

         [Authorize(Roles = "Administratorius")]
        public ActionResult Index()
        {
            var patogumaitarpines = db.PatogumaiTarpines.Include(p => p.Kambarys).Include(p => p.Patogumas);
            return View(patogumaitarpines.ToList());
        }

        [Authorize(Roles = "Administratorius")]
        public ActionResult Create()
        {
            ViewBag.kambarysID = new SelectList(db.Kambariai, "ID", "numeris");
            ViewBag.patogumasID = new SelectList(db.Patogumai, "ID", "pavadinimas");
            return View();
        }

        [Authorize(Roles = "Administratorius")]
        // creates a entry to patogumaitarpine
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,kambarysID,patogumasID")] PatogumaiTarpine patogumaitarpine)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    bool pasikartoja= db.PatogumaiTarpines.Where(model => model.kambarysID == patogumaitarpine.kambarysID && 
                        model.patogumasID == patogumaitarpine.patogumasID).Any();
                    if (pasikartoja)
                    {
                        ViewBag.ErrorMessage = "Kambarys su tokiu patogumu jau egzistuoja.";
                    }
                    else
                    {
                        db.PatogumaiTarpines.Add(patogumaitarpine);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Įvyko klaida. Bandykite dar kartą.");
            }

            ViewBag.kambarysID = new SelectList(db.Kambariai, "ID", "numeris", patogumaitarpine.kambarysID);
            ViewBag.patogumasID = new SelectList(db.Patogumai, "ID", "pavadinimas", patogumaitarpine.patogumasID);
            return View(patogumaitarpine);
        }


         [Authorize(Roles = "Administratorius")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PatogumaiTarpine patogumaitarpine = db.PatogumaiTarpines.Find(id);
            if (patogumaitarpine == null)
            {
                return HttpNotFound();
            }
            ViewBag.kambarysID = new SelectList(db.Kambariai, "ID", "numeris", patogumaitarpine.kambarysID);
            ViewBag.patogumasID = new SelectList(db.Patogumai, "ID", "pavadinimas", patogumaitarpine.patogumasID);
            return View(patogumaitarpine);
        }

         [Authorize(Roles = "Administratorius")]
        // edits information 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,kambarysID,patogumasID")] PatogumaiTarpine patogumaitarpine)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(patogumaitarpine).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Įvyko klaida. Bandykite dar kartą..");
            }
            ViewBag.kambarysID = new SelectList(db.Kambariai, "ID", "numeris", patogumaitarpine.kambarysID);
            ViewBag.patogumasID = new SelectList(db.Patogumai, "ID", "pavadinimas", patogumaitarpine.patogumasID);
            return View(patogumaitarpine);
        }

         [Authorize(Roles = "Administratorius")]
        public ActionResult Delete(int? id,  bool? saveChangesError = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Įvyko klaida. Bandykite dar kartą.";
            }
            PatogumaiTarpine patogumaitarpine = db.PatogumaiTarpines.Find(id);
            if (patogumaitarpine == null)
            {
                return HttpNotFound();
            }
            return View(patogumaitarpine);
        }

         [Authorize(Roles = "Administratorius")]
        // delete the romm
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                PatogumaiTarpine patogumaitarpine = db.PatogumaiTarpines.Find(id);
                db.PatogumaiTarpines.Remove(patogumaitarpine);
                db.SaveChanges();
            }
            catch (DataException)
            {
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administratorius")]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
