﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Viesbutis.Models.KambarysPosisteme;
using Viesbutis.DAL;

namespace Viesbutis.Controllers
{
    public class ApgyvendinimoTipasController : Controller
    {
        private ViesbutisContext db = new ViesbutisContext();

        [Authorize(Roles = "Administratorius")]

        public ActionResult Index()
        {
            return View(db.ApgyvendinimoTipai.ToList());
        }


       [Authorize(Roles = "Administratorius")]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize(Roles = "Administratorius")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,pavadinimas,aprasymas")] ApgyvendinimoTipas apgyvendinimotipas)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    bool pasikartojapav = db.ApgyvendinimoTipai.Where(model => model.pavadinimas == apgyvendinimotipas.pavadinimas).Any();
                    if (pasikartojapav)
                    {
                        ViewBag.ErrorMessage = "Apgyvendinimo tipas su tokiu pavadinimu jau egzistuoja.";
                    }
                    else
                    {
                        db.ApgyvendinimoTipai.Add(apgyvendinimotipas);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Neįmanoma išsaugoti pakeitimų. Pamėginkite dar kartą ir jei nepavyks susisiekite sus sistemos administratoriumi.");
            }

            return View(apgyvendinimotipas);
        }

        [Authorize(Roles = "Administratorius")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApgyvendinimoTipas apgyvendinimotipas = db.ApgyvendinimoTipai.Find(id);
            if (apgyvendinimotipas == null)
            {
                return HttpNotFound();
            }
            return View(apgyvendinimotipas);
        }

       [Authorize(Roles = "Administratorius")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,pavadinimas,aprasymas")] ApgyvendinimoTipas apgyvendinimotipas)
        {
            try
            {
                if (ModelState.IsValid)
                {
                   
                        db.Entry(apgyvendinimotipas).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Neįmanoma išsaugoti pakeitimų. Pamėginkite dar kartą ir jei nepavyks susisiekite sus sistemos administratoriumi.");
            }
            return View(apgyvendinimotipas);
        }

        [Authorize(Roles = "Administratorius")]
        public ActionResult Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Ištrinti nepavyko.Try again, and if the problem persists see your system administrator.";
            }
            ApgyvendinimoTipas apgyvendinimotipas = db.ApgyvendinimoTipai.Find(id);
            if (apgyvendinimotipas == null)
            {
                return HttpNotFound();
            }
            return View(apgyvendinimotipas);
        }

       [Authorize(Roles = "Administratorius")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                ApgyvendinimoTipas apgyvendinimotipas = db.ApgyvendinimoTipai.Find(id);
                bool panaudotas = db.Kambariai.Where(model => model.apgyvendinimoTipasID == id).Any();
                if (panaudotas)
                {
                    ViewBag.ErrorMessage = "Yra kambarys su tokiu apgyvendinimo tipu.";
                    return View(apgyvendinimotipas);
                }
                db.ApgyvendinimoTipai.Remove(apgyvendinimotipas);
                db.SaveChanges();
            }
            catch (DataException/* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administratorius")]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
