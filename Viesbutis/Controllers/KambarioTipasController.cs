﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Viesbutis.Models.KambarysPosisteme;
using Viesbutis.DAL;

namespace Viesbutis.Controllers
{
    public class KambarioTipasController : Controller
    {
        private ViesbutisContext db = new ViesbutisContext();

        [Authorize(Roles = "Administratorius")]
        public ActionResult Index()
        {
            return View(db.KambarioTipai.ToList());
        }



       [Authorize(Roles = "Administratorius")]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize(Roles = "Administratorius")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,trumpinys,pavadinimas,aprasymas")] KambarioTipas kambariotipas)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    bool pasikartojapav = db.KambarioTipai.Where(model => model.pavadinimas == kambariotipas.pavadinimas ||
                        model.trumpinys == kambariotipas.trumpinys).Any();
                    if (pasikartojapav)
                    {
                        ViewBag.ErrorMessage = "Kambario tipas su tokiu pavadinimu/ trumpiniu jau egzistuoja.";
                    }
                    else
                    {
                        db.KambarioTipai.Add(kambariotipas);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Neįmanoma išsaugoti pakeitimų. Pamėginkite dar kartą ir jei nepavyks susisiekite sus sistemos administratoriumi.");
            }

            return View(kambariotipas);
        }

        [Authorize(Roles = "Administratorius")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KambarioTipas kambariotipas = db.KambarioTipai.Find(id);
            if (kambariotipas == null)
            {
                return HttpNotFound();
            }
            return View(kambariotipas);
        }

        [Authorize(Roles = "Administratorius")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,trumpinys,pavadinimas,aprasymas")] KambarioTipas kambariotipas)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(kambariotipas).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            
           catch (DataException /* dex */)
           {
              //Log the error (uncomment dex variable name and add a line here to write a log.
              ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
           }
            return View(kambariotipas);
        }

       [Authorize(Roles = "Administratorius")]
        public ActionResult Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Ištrinti nepavyko.Try again, and if the problem persists see your system administrator.";
            }
            KambarioTipas kambariotipas = db.KambarioTipai.Find(id);
            if (kambariotipas == null)
            {
                return HttpNotFound();
            }
            return View(kambariotipas);
        }

       [Authorize(Roles = "Administratorius")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                KambarioTipas kambariotipas = db.KambarioTipai.Find(id);
                bool panaudotas = db.Kambariai.Where(model => model.kambarioTipasID== id).Any();
                if (panaudotas)
                {
                    ViewBag.ErrorMessage = "Yra kambarys su tokiu tipu.";
                    return View(kambariotipas);
                }
                db.KambarioTipai.Remove(kambariotipas);
                db.SaveChanges();
              
            }
            catch (DataException/* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administratorius")]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
