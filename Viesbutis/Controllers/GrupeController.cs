﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Viesbutis.DAL;
using Viesbutis.Models.KlientasPosisteme;
using Viesbutis.Models.KlientasPosisteme.ViewModels;

namespace Viesbutis.Controllers
{
    public class GrupeController : Controller
    {
        private ViesbutisContext _db = new ViesbutisContext();

        // returns the list of groups
        [Authorize(Roles = "Administratorius")]
        public ActionResult Index()
        {
            var grupes = _db.Grupes.OrderBy(x => x.pavadinimas);

            return View(grupes);
        }

        // returns the details of single group
        [Authorize(Roles = "Administratorius")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Grupe grupe = _db.Grupes.Find(id);
            if (grupe == null)
            {
                return HttpNotFound();
            }

            GrupeViewModel grupeViewModel = new GrupeViewModel() 
            {
                ID = grupe.ID,
                pavadinimas = grupe.pavadinimas,
                nuolaida = grupe.nuolaida,
                aprasymas = grupe.aprasymas
            };

            return View(grupeViewModel);
        }

        [Authorize(Roles = "Administratorius")]
        public ActionResult Create()
        {
            return View();
        }

        // creates a new group
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pavadinimas, nuolaida, aprasymas")]GrupeViewModel grupeViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Grupe grupe = new Grupe() 
                    {
                        pavadinimas = grupeViewModel.pavadinimas,
                        nuolaida = grupeViewModel.nuolaida,
                        aprasymas = grupeViewModel.aprasymas
                    };
                    _db.Grupes.Add(grupe);
                    _db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Įvyko klaida. Bandykite dar kartą.");
            }

            return View(grupeViewModel);
        }

        [Authorize(Roles = "Administratorius")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Grupe grupe = _db.Grupes.Find(id);
            if (grupe == null)
            {
                return HttpNotFound();
            }

            GrupeViewModel grupeViewModel = new GrupeViewModel() 
            {
                ID = grupe.ID,
                pavadinimas = grupe.pavadinimas,
                nuolaida = grupe.nuolaida,
                aprasymas = grupe.aprasymas
            };

            return View(grupeViewModel);
        }

        // edits the group information
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID, pavadinimas, nuolaida, aprasymas")]GrupeViewModel grupeViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Grupe grupe = new Grupe() 
                    {
                        ID = grupeViewModel.ID,
                        pavadinimas = grupeViewModel.pavadinimas,
                        nuolaida = grupeViewModel.nuolaida,
                        aprasymas = grupeViewModel.aprasymas
                    };
                    _db.Entry(grupe).State = EntityState.Modified;
                    _db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Įvyko klaida. Bandykite dar kartą.");
            }

            return View(grupeViewModel);
        }

        [Authorize(Roles = "Administratorius")]
        public ActionResult Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Įvyko klaida. Bandykite dar kartą.";
            }

            Grupe grupe = _db.Grupes.Find(id);
            if (grupe == null)
            {
                return HttpNotFound();
            }

            GrupeViewModel grupeViewModel = new GrupeViewModel() 
            {
                ID = grupe.ID,
                pavadinimas = grupe.pavadinimas,
                nuolaida = grupe.nuolaida,
                aprasymas = grupe.aprasymas
            };

            return View(grupeViewModel);
        }

        // deletes the group
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Grupe grupe = _db.Grupes.Find(id);
                if (grupe.Klientai.Count() == 0)
                {
                    _db.Entry(grupe).State = EntityState.Deleted;
                    _db.SaveChanges();
                }
                else
                {
                    ViewBag.ErrorMessage = "Grupės negalima ištrinti. Egzistuoja su ja susijusių vartotojų";
                    GrupeViewModel model = new GrupeViewModel
                    {
                        ID = grupe.ID,
                        pavadinimas = grupe.pavadinimas,
                        nuolaida = grupe.nuolaida,
                        aprasymas = grupe.aprasymas
                    };
                    return View(model);
                }
            }
            catch (DataException)
            {
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}