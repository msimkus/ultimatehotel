﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Viesbutis.DAL;
using Viesbutis.Models.MokejimasPosisteme;

namespace Viesbutis.Controllers
{
    public class MokejimoTipasController : Controller
    {
        private ViesbutisContext db = new ViesbutisContext();

        // GET: /Mokejimas/
        [Authorize(Roles="Buhalteris")]
        public ActionResult Index()
        {
            var mokejimoTipai = db.MokejimoTipai.OrderBy(m => m.pavadinimas);
            return View(mokejimoTipai.ToList());
        }

        [Authorize(Roles = "Buhalteris")]
        // GET: /Mokejimas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MokejimoTipas mokejimoTipas = db.MokejimoTipai.Find(id);
            if (mokejimoTipas == null)
            {
                return HttpNotFound();
            }

            return View(mokejimoTipas);
        }

        [Authorize(Roles = "Buhalteris")]
        // GET: /Mokejimas/Create
        public ActionResult Create()
        {
            return View();
        }

        [Authorize(Roles = "Buhalteris")]
        // POST: /mokejimoTipas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pavadinimas")] MokejimoTipas mokejimoTipas)
        {
            if (ModelState.IsValid)
            {
                db.MokejimoTipai.Add(mokejimoTipas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mokejimoTipas);
        }
        //---------------------------------------


        [Authorize(Roles = "Buhalteris")]
        // GET: /Mokejimas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MokejimoTipas mokejimoTipas = db.MokejimoTipai.Find(id);
            if (mokejimoTipas == null)
            {
                return HttpNotFound();
            }

            return View(mokejimoTipas);
        }

        [Authorize(Roles = "Buhalteris")]
        // POST: /Mokejimas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,pavadinimas")] MokejimoTipas mokejimoTipas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mokejimoTipas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mokejimoTipas);
        }
        [Authorize(Roles = "Buhalteris")]

        // GET: /Mokejimas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MokejimoTipas mokejimoTipas = db.MokejimoTipai.Find(id);
            if (mokejimoTipas == null)
            {
                return HttpNotFound();
            }
            return View(mokejimoTipas);
        }
        [Authorize(Roles = "Buhalteris")]
        // POST: /Mokejimas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MokejimoTipas mokejimoTipas = db.MokejimoTipai.Find(id);
            db.MokejimoTipai.Remove(mokejimoTipas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [Authorize(Roles = "Buhalteris")]      
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
	}
}